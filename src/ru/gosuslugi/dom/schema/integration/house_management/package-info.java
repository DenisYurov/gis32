@javax.xml.bind.annotation.XmlSchema(namespace = "http://dom.gosuslugi.ru/schema/integration/house-management/", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED,
        xmlns = {
                @javax.xml.bind.annotation.XmlNs(
                        prefix = "hous",
                        namespaceURI = "http://dom.gosuslugi.ru/schema/integration/house-management/"),
                @javax.xml.bind.annotation.XmlNs(
                        prefix = "nsi",
                        namespaceURI = "http://dom.gosuslugi.ru/schema/integration/nsi-base/"),
                @javax.xml.bind.annotation.XmlNs(
                        prefix = "soapenv",
                        namespaceURI = "http://schemas.xmlsoap.org/soap/envelope/"),
                @javax.xml.bind.annotation.XmlNs(
                        prefix = "base",
                        namespaceURI = "http://dom.gosuslugi.ru/schema/integration/base/"),
        }
)
package ru.gosuslugi.dom.schema.integration.house_management;
