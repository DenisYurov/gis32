package signature;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCP.KeyStore.JCPPrivateKeyEntry;


import javax.net.ssl.KeyManagerFactory;
import javax.xml.soap.SOAPMessage;
import java.io.InputStream;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.Properties;

import ru.CryptoPro.JCPxml.xmldsig.JCPXMLDSigInit;
import signature.demo.exceptions.ElementNotFoundException;
import signature.demo.xades.Consts;
import signature.demo.xades.production.CustomizableXadesBesSigningProfileFactory;
import signature.demo.xades.providers.CustomizableAlgorithmProvider;
import signature.demo.xades.providers.CustomizableMessageDigestEngineProvider;
import signature.demo.xml.IdResolver;
import xades4j.algorithms.EnvelopedSignatureTransform;
import xades4j.algorithms.ExclusiveCanonicalXMLWithoutComments;
import xades4j.production.*;
import xades4j.properties.DataObjectDesc;
import xades4j.providers.KeyingDataProvider;
import xades4j.providers.MessageDigestEngineProvider;
import xades4j.providers.impl.DirectKeyingDataProvider;

import static jaxws.jaxutil.toDocument;
import static soap.queries.utility.domToSoapMessage;

public class sign {
    public static Properties prop;

    public static SOAPMessage signatureString(SOAPMessage soapMsg)  throws Exception{
        String ALIAS = prop.getProperty("ALIAS"); //"04195224@2016-10-20-IOI EAO II ?EO";
            char[] PASSWORD = prop.getProperty("SIGN_PASSWORD").toCharArray();
        JCPXMLDSigInit.init();
        // инициализируем Apache Santuario
        org.apache.xml.security.Init.init();

// загружаем криптопровайдер
        Provider provider = new ru.CryptoPro.JCP.JCP();
        Security.addProvider(provider);

// загружаем хранилище закрытых ключей
        KeyStore keyStore = KeyStore.getInstance("HDImageStore", JCP.PROVIDER_NAME);
        keyStore.load(null, null);
        KeyManagerFactory kf = KeyManagerFactory.getInstance("GostX509");
        InputStream stream = null;
        keyStore.load(stream, PASSWORD);
        kf.init(keyStore, PASSWORD);

// загружаем закрытый ключ
        JCPPrivateKeyEntry keyEntry =
                (JCPPrivateKeyEntry) keyStore.getEntry(
                        ALIAS,
                        new KeyStore.PasswordProtection(PASSWORD));

// создаем провайдер для доступа к закрытому ключу
        KeyingDataProvider kp = new DirectKeyingDataProvider((X509Certificate) keyEntry.getCertificate(), keyEntry.getPrivateKey());

// создаем провайдер, описывающий используемые алгоритмы
        CustomizableAlgorithmProvider algorithmsProvider = new CustomizableAlgorithmProvider();
        algorithmsProvider.setSignatureAlgorithm(Consts.SIGNATURE_ALGORITHM);

        algorithmsProvider.setCanonicalizationAlgorithmForSignature(Consts.CANONICALIZATION_ALGORITHM_FOR_SIGNATURE);
        algorithmsProvider.setCanonicalizationAlgorithmForTimeStampProperties(Consts.CANONICALIZATION_ALGORITHM_FOR_TIMESTAMP_PROPERTIES);
        algorithmsProvider.setDigestAlgorithmForDataObjsReferences(Consts.DIGEST_ALGORITHM_URI);
        algorithmsProvider.setDigestAlgorithmForReferenceProperties(Consts.DIGEST_ALGORITHM_URI);
        algorithmsProvider.setDigestAlgorithmForTimeStampProperties(Consts.DIGEST_ALGORITHM_URI);

// создаем провайдер, ответственный за расчет хешей

        MessageDigestEngineProvider messageDigestEngineProvider = new CustomizableMessageDigestEngineProvider(Consts.DIGEST_ALGORITHM_NAME, provider);

// настраиваем профиль подписания

        XadesSigningProfile profile = new CustomizableXadesBesSigningProfileFactory()
                .withKeyingProvider(kp)
                .withMessageDigestEngineProvider(messageDigestEngineProvider)
                .withAlgorithmsProvider(algorithmsProvider)
                .create();

// создаем объект, ответственный за создание подписи
        XadesSigner signer = profile.newSigner();

// загружаем проверяемый XML-документ
        //Document document = XMLParser.parseXml(new File("C:\\Users\\dionis\\IdeaProjects\\signature\\test.xml"));
// Параметр soapMsg в Document
        Document document = toDocument(soapMsg);
// объявляем атрибут Id в качестве идентифицирующего
        IdResolver.resolveIds(document.getDocumentElement());

// ищем подписываемый элемент
        String signedElementId = "1";
        Element signedElement = document.getElementById(signedElementId);
        if (signedElement == null) {
            throw new ElementNotFoundException("Element to be signed not found: " + signedElementId);
        }

// ищем элемент, в который нужно поместить подпись; если не указан, помещаем подпись в подписываемый элемент

//String containerElementId = parameters.getContainerElementId() == null ? signedElementId : parameters.getContainerElementId();
        String containerElementId = signedElementId;
        Element signatureContainer = document.getElementById(containerElementId);//document.getDocumentElement();
        if (signatureContainer == null) {
            throw new ElementNotFoundException("Container element not found: " + containerElementId);
        }

// настраиваем подписываемые данные
        DataObjectDesc obj = new DataObjectReference('#' + signedElementId);
        obj.withTransform(new EnvelopedSignatureTransform());

        if (containerElementId.equals(signedElementId)) {
// если подпись помещается в подписываемый элемент, применяем трансформацию enveloped signature transform
// если этого не сделать, подпись нельзя будет проверить
            obj.withTransform(new EnvelopedSignatureTransform());
        }

// применяем трансформацию Exclusive XML Canonicalization 1.0 without comments (комментарии исключаются из подписываемых данных)
        obj.withTransform(new ExclusiveCanonicalXMLWithoutComments());

// создаем подпись

        SignedDataObjects dataObjs = new SignedDataObjects(obj);
        signer.sign(dataObjs, signatureContainer, SignatureAppendingStrategies.AsFirstChild);

        return domToSoapMessage(document);
        /*
// выводим результат в stdout
        System.out.println(XMLPrinter.toString(document));
// выводим результат в файл
        byte[] xmlBytes = XMLPrinter.toBytes(document);
        FileOutputStream fos = new FileOutputStream("C:\\Users\\dionis\\IdeaProjects\\signature\\signedtest.xml");
        fos.write(xmlBytes);
        fos.flush();
        fos.close();
*/
    }

}

