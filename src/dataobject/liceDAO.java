package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Properties;

import static database.database.getGGGGNow;
import static database.database.getMMNow;

/**
 * Created by dionis on 05.12.2016.
 */
public class liceDAO {
    public static Properties prop;
    public static String mmggMonthBack = getMMNow(-1) + getGGGGNow(-1).substring(2);;
    public static ResultSet getLiceByAccount(String account) {
        try {
            Connection conn = database.dbfJDBCConnPath("/gilcom");
            String mmgg = mmggMonthBack;

            String sql = "SELECT lice.*,dol.skt,sod.tar as sod_tar,pvkv.k1 recalc FROM lice.lice"+mmgg+" AS lice " +
                    "LEFT JOIN sal.dol"+mmgg+" as dol ON dol.ln = lice.ln " +
                    "LEFT JOIN kpv.pvkv"+mmgg+" as pvkv ON lice.ln = pvkv.ln " +
                    "LEFT JOIN tar.sod_rem as sod ON lice.kod_sod = sod.kod "+
                    "WHERE trim(ln) = ? AND sod.gm = ?";
            PreparedStatement pstat = conn.prepareStatement(sql);
            pstat.setString(1, account.trim());
            pstat.setString(2,prop.getProperty("gmSod_remTAR"));
            return pstat.executeQuery();

            /*
            String sql = "SELECT lice.* FROM lice.lice"+mmgg+" AS lice " +
                    "WHERE ln = "+account;
            return database.executeSQL(conn,sql);
            */
        } catch (Exception e) {
            System.err.println("Error occurred in getLiceByAccount");
            e.printStackTrace();
            return null;
        }
    }
    public static ResultSet getLice() {
        try {
            Connection conn = database.dbfJDBCConnPath("/gilcom");
            String mmgg = mmggMonthBack;
            String sql = "SELECT lice.ln FROM lice.lice"+mmgg+" AS lice";
            PreparedStatement pstat = conn.prepareStatement(sql);
            ResultSet rs = pstat.executeQuery();
            return rs;
        } catch (Exception e) {
            System.err.println("Error occurred in getHouse");
            e.printStackTrace();
            return null;
        }
    }
    public static int getLiceCount() {
        try {
            Connection conn = database.dbfJDBCConnPath("/gilcom");
            PreparedStatement pstat = conn.prepareStatement("SELECT COUNT(*) AS cnt  FROM lice.lice"+mmggMonthBack);
            ResultSet rs = pstat.executeQuery();
            rs.next();
            return rs.getInt("cnt");
        } catch (Exception e) {
            System.err.println("Error occurred in getAccountCount");
            e.printStackTrace();
            return 0;
        }
    }

}
