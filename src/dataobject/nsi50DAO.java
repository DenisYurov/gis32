package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by dionis on 12.12.2016.
 */
public class nsi50DAO {
    public static void deleteAllJilus50() throws Exception {
        Connection conn = database.dbfJDBCConnection();
        PreparedStatement pstat = conn.prepareStatement("DELETE FROM jilus50 ");
        pstat.executeUpdate();
        conn.close();
    }
    public static ResultSet getNsi50AllItem() {
        try{
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT * FROM jilus50 ");
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getNsi50AllItem");
            e.printStackTrace();
            return null;
        }
    }
    public static void insertNsi50(String code, String guid, String modified, boolean isActual, String value) throws Exception {
        String sql = "INSERT INTO jilus50 (code,guid,modified,isactual,value) VALUES (?,?,?,?,?)";
        Connection conn = database.dbfJDBCConnection();
        PreparedStatement pstat = conn.prepareStatement(sql);
        pstat.setString(1, code);
        pstat.setString(2, guid);
        pstat.setString(3, modified);
        pstat.setBoolean(4, isActual);
        pstat.setString(5, value);
        pstat.executeUpdate();
        conn.close();
    }
}
