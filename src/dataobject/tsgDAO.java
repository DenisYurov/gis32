package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;

import static database.database.getGGGGNow;
import static database.database.getMMNow;

/**
 * Created by dionis on 07.12.2016.
 */
public class tsgDAO {
    private static String gm =  getGGGGNow(-1) + getMMNow(-1);
    public static void clearTsgByGm() throws Exception{
        Connection conn = database.dbfJDBCConnPath("/gilcom");
        PreparedStatement pstat = conn.prepareStatement("DELETE FROM tsg WHERE gm<>? ");
        pstat.setString(1,gm);
        pstat.executeUpdate();

    }
}
