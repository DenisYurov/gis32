package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by dionis on 10.12.2016.
 */
public class ot_muszDAO {
    public static int getOt_muszRecCount() {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT COUNT(*) AS cnt FROM gilcom.ot_musz");
            ResultSet rs = pstat.executeQuery();
            rs.next();
            return rs.getInt("cnt");
        } catch (Exception e) {
            System.err.println("Error occurred in getOrg51RecCount");
            e.printStackTrace();
            return 0;
        }
    }

}
