package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static dataobject.billdocDAO.getBillByAccounNum;
import static dataobject.tsgDAO.clearTsgByGm;

/**
 * Created by dionis on 07.12.2016.
 */
public class accgilcomerrDAO {
    public static void insertAccGilcomErr(String ln, String err) {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("INSERT INTO accgilcomerr (ln,error) VALUES(?,?)");
            pstat.setString(1, ln.trim());
            pstat.setString(2, err);
            pstat.executeUpdate();
            conn.close();


        } catch (Exception e) {
            System.err.println("Error occurred in insertBadAccount");
            e.printStackTrace();
        }
    }
    public static ResultSet getAccGilcomErr() {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT * FROM accgilcomerr");
            ResultSet rs = pstat.executeQuery();
            return rs;
        } catch (Exception e) {
            System.err.println("Error occurred in getAccGilcomErr");
            e.printStackTrace();
            return null;
        }
    }
    public static void deleteAccGilcomErr() throws Exception{
        Connection conn = database.dbfJDBCConnection();
        PreparedStatement pstat = conn.prepareStatement("DELETE FROM accgilcomerr");
        pstat.executeUpdate();

    }

}
