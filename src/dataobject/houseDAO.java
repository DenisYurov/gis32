package dataobject;

import database.database;
import javafx.beans.property.SimpleDoubleProperty;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;

import static database.database.dbfJDBCConnectionDos;
import static database.database.getGGGGNow;
import static database.database.getMMNow;
import static dataobject.orgDAO.getOrgRecCount;
import static dataobject.ot_muszDAO.getOt_muszRecCount;

/**
 * Created by dionis on 28.11.2016.
 */
public class houseDAO {
    private static double progressDelta;
    //положение прогрессбара
    public static volatile SimpleDoubleProperty progress;
    public static int getHouseCount() {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT COUNT(*) AS cnt FROM house");
            ResultSet rs = pstat.executeQuery();
            rs.next();
            return rs.getInt("cnt");
        } catch (Exception e) {
            System.err.println("Error occurred in getOrg51RecCount");
            e.printStackTrace();
            return 0;
        }
    }

    public static ResultSet getHouse() {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT house.*,org.name FROM house LEFT JOIN org ON house.orgppaguid = org.orgppaguid");
            ResultSet rs = pstat.executeQuery();
            return rs;
        } catch (Exception e) {
            System.err.println("Error occurred in getHouse");
            e.printStackTrace();
            return null;
        }
    }

    public static void insertGilcomHouse() {
        //проходим по организациям поставщикам из org, по таблице ot_musz,tsg,fhome_fias,haracter
        //заносим найденные дома в house
        String gmMonthBack = getGGGGNow(-1)+ getMMNow(-1);
        try {
            // Вычисление дельты прогрессбара
            progressDelta = (1.0/getOrgRecCount(true));
            ResultSet dataProwider = dataobject.orgDAO.getAllOrg(true);
            Connection conn = database.dbfJDBCConnection();
            Connection connDos = dbfJDBCConnectionDos();
            PreparedStatement pstat;
            pstat = conn.prepareStatement("DELETE FROM house;");
            pstat.executeUpdate();

            while (dataProwider.next()) {
                pstat = conn.prepareStatement("SELECT ot_musz FROM gilcom.ot_musz WHERE trim(inn) = ?");
                pstat.setString(1, dataProwider.getString("inn").trim());
                ResultSet ot_musz = pstat.executeQuery();
                while (ot_musz.next()) {
                    pstat = conn.prepareStatement("SELECT jeu FROM gilcom.tsg WHERE ot_musz = ? AND gm = ?");
                    pstat.setString(1, ot_musz.getString("ot_musz"));
                    pstat.setString(2, gmMonthBack);
                    ResultSet tsg = pstat.executeQuery();
                    while (tsg.next()) {
                        String sql = "SELECT fhome.houseguid, har.exp_2 FROM gilcom.fhome_fias AS fhome " +
                                "LEFT JOIN gilcom.haracter AS har ON har.knom = fhome.knom WHERE jeu =?";
                        pstat = connDos.prepareStatement(sql);
                        pstat.setString(1, tsg.getString("jeu"));
                        ResultSet fhome_fias = pstat.executeQuery();
                        while (fhome_fias.next()) {
                            pstat = conn.prepareStatement("INSERT INTO house (orgppaguid,fiasguid,address) VALUES (?,?,?)");
                            pstat.setString(1, dataProwider.getString("orgppaguid"));
                            pstat.setString(2, fhome_fias.getString("houseguid"));
                            pstat.setString(3, fhome_fias.getString("exp_2"));
                            pstat.executeUpdate();
                        }
                    }
                }
                progress.set(progress.get() + progressDelta);
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Error occurred in getHouse");
            e.printStackTrace();
            //return null;
        }
    }

}
