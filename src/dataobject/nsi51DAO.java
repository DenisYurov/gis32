package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by dionis on 21.11.2016.
 */
public class nsi51DAO {
    public static ResultSet getNsi51Item(String orgPPAGUID) {
        try{
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT n51.code,n51.guid,n51.orgppaguid,n51.value,org.name AS name FROM nsiloc51 AS n51 LEFT JOIN org ON org.orgppaguid = n51.orgppaguid  WHERE n51.orgppaguid = ?");
            pstat.setString(1, orgPPAGUID);
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getNsi51Item");
            e.printStackTrace();
            return null;
        }
    }
    public static ResultSet getNsi51AllItem() {
        try{
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT n51.code,n51.guid,n51.orgppaguid,n51.value,org.name AS name FROM nsiloc51 AS n51 LEFT JOIN org ON org.orgppaguid = n51.orgppaguid ");
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getNsi51AllItem");
            e.printStackTrace();
            return null;
        }
    }
    public static int getNsi51RecCount() {
        try{
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT COUNT(*) AS cnt FROM nsiloc51");
            ResultSet rs =  pstat.executeQuery();
            rs.next();
            return rs.getInt("cnt");
        } catch (Exception e) {
            System.err.println("Error occurred in getNsi51RecCount");
            e.printStackTrace();
            return 0;
        }
    }
    public static ResultSet getNsi51Item1(String orgPPAGUID, String value) {
        try{
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT n51.code,n51.guid,n51.orgppaguid,n51.value AS name FROM nsiloc51 AS n51 WHERE n51.orgppaguid = ? AND trim(value) = ?");
            pstat.setString(1, orgPPAGUID);
            pstat.setString(2, value);
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getNsi51Item1");
            e.printStackTrace();
            return null;
        }
    }
    public static void insertNsi51(String code, String guid, String modified, boolean isActual, String value) throws Exception {
        String sql = "INSERT INTO nsiloc51 (code,guid,modified,isactual,value) VALUES (?,?,?,?,?)";
        Connection conn = database.dbfJDBCConnection();
        PreparedStatement pstat = conn.prepareStatement(sql);
        pstat.setString(1, code);
        pstat.setString(2, guid);
        pstat.setString(3, modified);
        pstat.setBoolean(4, isActual);
        pstat.setString(5, value);
        pstat.executeUpdate();
        conn.close();
    }
    public static void deleteJilus51All() throws Exception {
        Connection conn = database.dbfJDBCConnection();
        PreparedStatement pstat = conn.prepareStatement("DELETE FROM nsiloc51");
        //pstat.setString(1, orgppaguid);
        pstat.executeUpdate();
        conn.close();
    }
}
