package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

/**
 * Created by dionis on 06.12.2016.
 */
public class municservDAO {
    public static Properties prop;
    public static String mmggMonthBack;

    public static String accoguid;
    public  static String nsicode;
    public  static String nsiguid;
    public  static Double rate;
    public  static Double totalpay;
    public  static Double acctotal;
    public  static Double recalc;
    public  static String recalcreas;
    public  static Double recalcsum;
    public  static String cnsindmetd;
    public  static Double cnsindval;
    public  static String cnsobhmetd;
    public  static Double cnsobshval;
    public  static String explan;
    public  static Double sinfindval;
    public  static Double sinfhonval;
    public  static Double sinfhotin;
    public  static Double sinfhotnee;
    public  static Double sinfhonnr;
    public  static Double sinfincnor;
    public static void initialize(){
        accoguid = "";
        nsicode = "";
        nsiguid = "";
        rate = 0.0;
        totalpay = 0.0;
        acctotal = 0.0;
        recalc = 0.0;
        recalcreas = "";
        recalcsum = 0.0;
        cnsindmetd = "";
        cnsindval = 0.0;
        cnsobhmetd = "";
        cnsobshval = 0.0;
        explan = "";
        sinfindval = 0.0;
        sinfhonval = 0.0;
        sinfhotin = 0.0;
        sinfhotnee = 0.0;
        sinfhonnr = 0.0;
        sinfincnor = 0.0;
    }
    public static ResultSet getMunicServ(String accountGUID) {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT * FROM bill.municserv WHERE TRIM(accoguid) = ?");
            pstat.setString(1, accountGUID);
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getMunicServ");
            e.printStackTrace();
            return null;
        }
    }
    public static  void insertServ() throws Exception{

        String sql = "INSERT INTO bill.municserv (accoguid, nsicode, nsiguid, rate, totalpay," +
                " acctotal, recalc, recalcreas, recalcsum, cnsindmetd, cnsindval, cnsobhmetd, cnsobshval, explan," +
                " sinfindval, sinfhonval, sinfhotin, sinfhotnee,sinfhonnr,sinfincnor ) " +
                " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement pstat = database.conn.prepareStatement(sql);
        pstat.setString(1, accoguid);
        pstat.setString(2, nsicode);
        pstat.setString(3,nsiguid);
        pstat.setDouble(4,rate);
        pstat.setDouble(5,totalpay);
        pstat.setDouble(6,acctotal);
        pstat.setDouble(7,recalc);
        pstat.setString(8,recalcreas);
        pstat.setDouble(9,recalcsum);
        pstat.setString(10,cnsindmetd);
        pstat.setDouble(11,cnsindval);
        pstat.setString(12,cnsobhmetd);
        pstat.setDouble(13,cnsobshval);
        pstat.setString(14,explan);
        pstat.setDouble(15,sinfindval);
        pstat.setDouble(16,sinfhonval);
        pstat.setDouble(17,sinfhotin);
        pstat.setDouble(18,sinfhotnee);
        pstat.setDouble(19,sinfhonnr);
        pstat.setDouble(20,sinfincnor);
        pstat.executeUpdate();
    }
    public static void deleteAllMunicServ() throws Exception{
        Connection conn = database.dbfJDBCConnection();
        PreparedStatement pstat = conn.prepareStatement("DELETE FROM bill.municserv");
        pstat.executeUpdate();
        conn.close();
    }
    public static void deleteMunicServ(String accguid) throws Exception {
        try {
            PreparedStatement pstat = database.conn.prepareStatement("DELETE FROM bill.municserv WHERE accoguid = ?");
            pstat.setString(1, accguid);
            pstat.executeUpdate();
        } catch (Exception e) {
            System.err.println("Error occurred in deleteMunicServ");
            e.printStackTrace();
        }

    }
    //справочник тарифов отопления
    public static ResultSet getFotpl(int kotpl) throws Exception{
        Connection conn = database.dbfJDBCConnPath("/gilcom");
        PreparedStatement pstat = conn.prepareStatement("SELECT * FROM tar.fotpl WHERE kotpl = ? AND gm = ?");
        pstat.setInt(1, kotpl);
        pstat.setString(2,mmggMonthBack);
        return pstat.executeQuery();
    }
    //справочник тарифов холодной воды ИП
    public static ResultSet getVidvod(int vidvod_s) throws Exception{
        Connection conn = database.dbfJDBCConnPath("/gilcom");
        PreparedStatement pstat = conn.prepareStatement("SELECT * FROM tar.vidvod WHERE vidvod = ? AND gm = ?");
        pstat.setInt(1, vidvod_s);
        pstat.setString(2,prop.getProperty("gmVidvodTAR"));
        return pstat.executeQuery();
    }
    //справочник тарифов холодной воды ОДН
    public static ResultSet getDomvod(int domvod) throws Exception{
        Connection conn = database.dbfJDBCConnPath("/gilcom");
        PreparedStatement pstat = conn.prepareStatement("SELECT * FROM tar.domvod WHERE domvod = ? AND gm = ?");
        pstat.setInt(1, domvod);
        pstat.setString(2,prop.getProperty("gmDomvodTAR"));
        return pstat.executeQuery();
    }
    //справочник тарифов горячая вода ИП
    public static ResultSet getVidgor(int vidgor) throws Exception{
        Connection conn = database.dbfJDBCConnPath("/gilcom");
        PreparedStatement pstat = conn.prepareStatement("SELECT * FROM tar.vidgor WHERE vidgor = ? AND gm = ?");
        pstat.setInt(1, vidgor);
        pstat.setString(2,prop.getProperty("gmVidgorTAR"));
        return pstat.executeQuery();
    }
    //справочник тарифов горячая вода ОДН
    public static ResultSet getDomgor(int domgor) throws Exception{
        Connection conn = database.dbfJDBCConnPath("/gilcom");
        PreparedStatement pstat = conn.prepareStatement("SELECT * FROM tar.domgor WHERE domgor = ? AND gm = ?");
        pstat.setInt(1, domgor);
        pstat.setString(2,prop.getProperty("gmDomgorTAR"));
        return pstat.executeQuery();
    }
    //справочник тарифов электроэнергия ИП
    public static ResultSet getFenerg(int pe) throws Exception{
        Connection conn = database.dbfJDBCConnPath("/gilcom");
        PreparedStatement pstat = conn.prepareStatement("SELECT * FROM tar.fenerg WHERE pe = ? AND gm = ?");
        pstat.setInt(1, pe);
        pstat.setString(2,prop.getProperty("gmFenergTAR"));
        return pstat.executeQuery();
    }
    //справочник тарифов электроэнергия ИП
    public static ResultSet getDompe(int dompe) throws Exception{
        Connection conn = database.dbfJDBCConnPath("/gilcom");
        PreparedStatement pstat = conn.prepareStatement("SELECT * FROM tar.dompe WHERE dompe = ? AND gm = ?");
        pstat.setInt(1, dompe);
        pstat.setString(2,prop.getProperty("gmDompeTAR"));
        return pstat.executeQuery();
    }
}
