package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by dionis on 14.12.2016.
 */
public class billsoaplogDAO {
    public static void insertbillsoaplogDAOErr(String orgPPAGUID, String messageGuid, String errorMsg) throws Exception {
        Connection conn = database.dbfJDBCConnection();
        PreparedStatement pstat = conn.prepareStatement("INSERT INTO billsoaplog (messguid,orgppaguid,error) VALUES (?,?,?)");
        pstat.setString(1, messageGuid);
        pstat.setString(2, orgPPAGUID);
        pstat.setString(3, errorMsg);
        pstat.executeUpdate();
        conn.close();
    }

    //очистка журнала целых сообщений
    public static void deleteAllBillSoapLog() throws Exception {
        Connection conn = database.dbfJDBCConnection();
        PreparedStatement pstat = conn.prepareStatement("DELETE FROM billsoaplog ");
        pstat.executeUpdate();

    }

    public static ResultSet getBillSoapLog(String orgPPAGUID) {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT * FROM billsoaplog WHERE TRIM(orgppaguid) = ?");
            pstat.setString(1, orgPPAGUID);
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getBillSoapLog");
            e.printStackTrace();
            return null;

        }
    }
}
