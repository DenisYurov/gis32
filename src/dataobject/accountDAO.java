package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by dionis on 29.11.2016.
 */
public class accountDAO {
    public static ResultSet getAccount() {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT org.name, account.* FROM account LEFT JOIN org ON account.orgppaguid = org.orgppaguid");
            ResultSet rs = pstat.executeQuery();
            return rs;
        } catch (Exception e) {
            System.err.println("Error occurred in getAccount");
            e.printStackTrace();
            return null;
        }
    }
    public static int getAccountCount() {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT COUNT(*) AS cnt FROM account");
            ResultSet rs = pstat.executeQuery();
            rs.next();
            return rs.getInt("cnt");
        } catch (Exception e) {
            System.err.println("Error occurred in getAccountCount");
            e.printStackTrace();
            return 0;
        }
    }
    public static ResultSet getAccByAccounNum(String accountnum) {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT org.name FROM account WHERE accountnum = ?");
            pstat.setString(1,accountnum);
            ResultSet rs = pstat.executeQuery();
            return rs;
        } catch (Exception e) {
            System.err.println("Error occurred in getAccByAccounNum");
            e.printStackTrace();
            return null;
        }
    }
}
