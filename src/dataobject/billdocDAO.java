package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by dionis on 21.11.2016.
 */
public class billdocDAO {
    public static ResultSet getAllBill() {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT * FROM bill.billdoc ");
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getAllBill");
            e.printStackTrace();
            return null;
        }
    }

    public static void deleteAllBill() throws Exception {
        Connection conn = database.dbfJDBCConnection();
        PreparedStatement pstat = conn.prepareStatement("DELETE FROM bill.billdoc ");
        pstat.executeUpdate();

    }

    public static void deleteBill(String accguid) throws Exception {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("DELETE FROM bill.billdoc WHERE accoguid = ?");
            pstat.setString(1, accguid);
            pstat.executeUpdate();
        } catch (Exception e) {
            System.err.println("Error occurred in getBillbyOrg");
            e.printStackTrace();
        }

    }

    public static ResultSet getBillbyOrgNData(String orgPPAGUID, int month, int year) {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT * FROM bill.billdoc WHERE orgppaguid = ? AND month = ? AND year =? ORDER BY year,month");
            pstat.setString(1, orgPPAGUID);
            pstat.setInt(2, month);
            pstat.setInt(3, year);
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getBillbyOrg");
            e.printStackTrace();
            return null;
        }
    }

    public static ResultSet getOrgDataNotSentBill() {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT orgppaguid, month, year FROM bill.billdoc GROUP BY orgppaguid, month, year");
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getBillRecCount");
            e.printStackTrace();
            return null;
        }
    }

    public static int getBillRecCount() {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT COUNT(*) AS cnt FROM bill.billdoc");
            ResultSet rs = pstat.executeQuery();
            rs.next();
            return rs.getInt("cnt");
        } catch (Exception e) {
            System.err.println("Error occurred in getOrgBill");
            e.printStackTrace();
            return 0;
        }
    }

    public static void insertBill(String orgPPAGUID, String accoguid, int livingpers, Double debt, Double totalsquar) throws Exception {
        PreparedStatement pstat = database.conn.prepareStatement("INSERT INTO bill.billdoc (orgppaguid, accoguid, month, year, livingpers, debt, totalsquar) VALUES (?,?,?,?,?,?,?)");
        pstat.setString(1, orgPPAGUID);
        pstat.setString(2, accoguid);
        pstat.setInt(3, getGorMNow(false));//month
        pstat.setInt(4, getGorMNow(true));
        pstat.setInt(5, livingpers);
        pstat.setDouble(6, debt);
        pstat.setDouble(7, totalsquar);
        pstat.executeUpdate();
    }

    public static ResultSet getBillByAccounNum(String accountnum) {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT account.accountnum FROM bill.billdoc AS bill JOIN account ON account.accguid = bill.accoguid WHERE account.accountnum = ?");
            pstat.setString(1, accountnum);
            ResultSet rs = pstat.executeQuery();
            return rs;
        } catch (Exception e) {
            System.err.println("Error occurred in getBillByAccounNum");
            e.printStackTrace();
            return null;
        }
    }

    public static int getGorMNow(boolean yearr) {
        Date date = new Date(); // now date
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if (yearr) {
            return cal.get(Calendar.YEAR);
        } else {
            return cal.get(Calendar.MONTH);//month is 0 based, so there current month - 1
        }
    }

}
