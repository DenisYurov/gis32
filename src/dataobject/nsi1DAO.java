package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by dionis on 21.11.2016.
 */
public class nsi1DAO {
        public static ResultSet getNsi1Item(String orgPPAGUID) {
            try{
                Connection conn = database.dbfJDBCConnection();
                PreparedStatement pstat = conn.prepareStatement("SELECT n1.code,n1.guid,n1.orgppaguid,n1.value,org.name AS name FROM nsiloc1 AS n1 LEFT JOIN org ON org.orgppaguid = n1.orgppaguid  WHERE n1.orgppaguid = ?");
                pstat.setString(1, orgPPAGUID);
                return pstat.executeQuery();
            } catch (Exception e) {
                System.err.println("Error occurred in getNsi1Item");
                e.printStackTrace();
                return null;
            }
        }
        public static ResultSet getNsi1AllItem() {
            try{
                Connection conn = database.dbfJDBCConnection();
                PreparedStatement pstat = conn.prepareStatement("SELECT n1.code,n1.guid,n1.orgppaguid,n1.value,org.name AS name FROM nsiloc1 AS n1 LEFT JOIN org ON org.orgppaguid = n1.orgppaguid ");
                return pstat.executeQuery();
            } catch (Exception e) {
                System.err.println("Error occurred in getNsi1AllItem");
                e.printStackTrace();
                return null;
            }
        }
        public static int getNsi1RecCount() {
            try{
                Connection conn = database.dbfJDBCConnection();
                PreparedStatement pstat = conn.prepareStatement("SELECT COUNT(*) AS cnt FROM nsiloc1");
                ResultSet rs =  pstat.executeQuery();
                rs.next();
                return rs.getInt("cnt");
            } catch (Exception e) {
                System.err.println("Error occurred in getNsi1RecCount");
                e.printStackTrace();
                return 0;
            }
        }

}
