package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

/**
 * Created by dionis on 23.11.2016.
 */
public class jilservDAO {
    public static Properties prop;
    public static ResultSet getJilServ(String accoguid) {
        try{
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT * FROM bill.jilserv WHERE TRIM(accoguid) = ?");
            pstat.setString(1, accoguid);
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getJilServ");
            e.printStackTrace();
            return null;
        }
    }
    public static void insertJilServ(String accoguid,int nsicode,String nsiguid,Double rate, Double totalpay, Double acctotal, Double cnsindval, Double cnsobshval,String explan, Double recalc) throws Exception{

            PreparedStatement pstat = database.conn.prepareStatement("INSERT INTO bill.jilserv (accoguid, nsicode, nsiguid, rate, totalpay, acctotal, cnsindval, cnsobshval, explan, recalc) VALUES(?,?,?,?,?,?,?,?,?,?)");
            pstat.setString(1, accoguid);
            pstat.setInt(2, nsicode);
            pstat.setString(3,nsiguid);
            pstat.setDouble(4,rate);
            pstat.setDouble(5,totalpay);
            pstat.setDouble(6,acctotal);
            pstat.setDouble(7,cnsindval);
            pstat.setDouble(8,cnsobshval);
            pstat.setString(9,explan);
            pstat.setDouble(10,recalc);
            pstat.executeUpdate();

    }
    public static ResultSet getJilUs50(int code) {
        try{
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT * FROM jilus50 WHERE code = ?");
            pstat.setInt(1, code);
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getJilUs50");
            e.printStackTrace();
            return null;
        }
    }
    //справочник тарифов наема
    public static ResultSet getFnaem(int kod_naem) throws Exception{
            Connection conn = database.dbfJDBCConnPath("/gilcom");
            PreparedStatement pstat = conn.prepareStatement("SELECT * FROM tar.fnaem WHERE kod_naem = ? AND gm = ?");
            pstat.setInt(1, kod_naem);
            pstat.setString(2,prop.getProperty("gmKod_naemTAR"));
            return pstat.executeQuery();
    }

    public static void deleteAllJilServ() throws Exception{
        Connection conn = database.dbfJDBCConnection();
        PreparedStatement pstat = conn.prepareStatement("DELETE FROM bill.jilserv ");
        pstat.executeUpdate();
        conn.close();

    }
    public static void deleteJilServ(String accguid) throws Exception {
        try {
            PreparedStatement pstat = database.conn.prepareStatement("DELETE FROM bill.jilserv WHERE accoguid = ?");
            pstat.setString(1, accguid);
            pstat.executeUpdate();
        } catch (Exception e) {
            System.err.println("Error occurred in deleteMunicServ");
            e.printStackTrace();
        }

    }
}
