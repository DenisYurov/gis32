package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by dionis on 21.11.2016.
 */
public class orgDAO {
    public static ResultSet getAllOrg(boolean showDataProvider) {
        //showDataProvider  true - Показывать только наших поставщиков информации
        try {
            Connection conn = database.dbfJDBCConnection();
            String sql;
            if (showDataProvider) {
                sql = "SELECT * FROM org WHERE provider = true";
            } else {
                sql = "SELECT * FROM org";
            }
            PreparedStatement pstat = conn.prepareStatement(sql);
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getAllOrg");
            e.printStackTrace();
            return null;
        }
    }


    public static ResultSet getOrg(String orgPPAGUID) {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT * FROM org WHERE orgppaguid = ?");
            pstat.setString(1, orgPPAGUID);
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getOrg");
            e.printStackTrace();
            return null;
        }
    }

    public static int getOrgRecCount(boolean showDataProvider) {
        //showDataProvider  true - Показывать только наших поставщиков информации
        try {
            Connection conn = database.dbfJDBCConnection();
            String sql;
            if (showDataProvider) {
                sql = "SELECT COUNT(*) AS cnt FROM org WHERE provider = true";
            } else {
                sql = "SELECT COUNT(*) AS cnt FROM org";
            }
            PreparedStatement pstat = conn.prepareStatement(sql);
            ResultSet rs = pstat.executeQuery();
            rs.next();
            return rs.getInt("cnt");
        } catch (Exception e) {
            System.err.println("Error occurred in getOrg51RecCount");
            e.printStackTrace();
            return 0;
        }
    }

    public static int getOrgNoGuidRecCount() {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT COUNT(*) AS cnt FROM org WHERE orgppaguid = null");
            ResultSet rs = pstat.executeQuery();
            rs.next();
            return rs.getInt("cnt");
        } catch (Exception e) {
            System.err.println("Error occurred in getOrg51RecCount");
            e.printStackTrace();
            return 0;
        }
    }

    public static ResultSet getOrgnoGuid() {
        try {
            Connection conn = database.dbfJDBCConnection();
            PreparedStatement pstat = conn.prepareStatement("SELECT * FROM org WHERE orgppaguid = null");
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getOrgnoGuid");
            e.printStackTrace();
            return null;
        }
    }

    //устанавливаем флаг. организация - не поставщик данных
    public static void setDataProvider(String orgPPAGUID, boolean fl) {
        try {
            Connection conn = database.dbfJDBCConnection();
            String sql = "UPDATE org SET provider = ?  WHERE orgppaguid = ?";
            PreparedStatement pstat = conn.prepareStatement(sql);
            pstat.setBoolean(1, fl);
            pstat.setString(2, orgPPAGUID);
            pstat.executeUpdate();
        } catch (Exception e) {
            System.err.println("Error occurred setNoDataProvider");
            e.printStackTrace();
        }

    }
}
