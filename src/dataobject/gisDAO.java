package dataobject;

import database.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import static database.database.getGGGGNow;
import static database.database.getMMNow;

/**
 * Created by dionis on 08.12.2016.
 */
public class gisDAO {
    public static Properties prop;
    public static String mmggMonthBack = getMMNow(-1) + getGGGGNow(-1).substring(2);
    public static ResultSet getGisByAccount(String account) {
        try {
            Connection conn = database.dbfJDBCConnPath("/gilcom");
            String mmgg = mmggMonthBack;
            String sql = "SELECT * FROM gis_"+mmgg+" AS lice " +
                    " WHERE trim(ln) = ?";

            PreparedStatement pstat = conn.prepareStatement(sql);
            pstat.setString(1, account.trim());
            return pstat.executeQuery();

        } catch (Exception e) {
            System.err.println("Error occurred in getLiceByAccount");
            e.printStackTrace();
            return null;
        }
    }
    public static ResultSet getHvodByAccount(String account) {
        try {
            Connection conn = database.dbfJDBCConnPath("/gilcom");
            String mmgg = mmggMonthBack;
            String sql = "SELECT * FROM meter.hvod"+mmgg+
                    " WHERE trim(ln) = ?";
            PreparedStatement pstat = conn.prepareStatement(sql);
            pstat.setString(1, account.trim());
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getHvodByAccount");
            e.printStackTrace();
            return null;
        }
    }
    public static ResultSet getHvByAccount(String account) {
        try {
            Connection conn = database.dbfJDBCConnPath("/gilcom");
            String mmgg = mmggMonthBack;
            String sql = "SELECT * FROM meter.hv"+mmgg+
                    " WHERE trim(ln) = ?";
            PreparedStatement pstat = conn.prepareStatement(sql);
            pstat.setString(1, account.trim());
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getHvByAccount");
            e.printStackTrace();
            return null;
        }
    }
    /*
    public static ResultSet getGisNCounters() {
        try {
            Connection conn = database.dbfJDBCConnPath("/gilcom");
            String mmgg = mmggMonthBack;
            String sql = "SELECT gis.ln, hvod.tek hvod_tek FROM gis_"+mmgg+ " AS gis"+
                    "LEFT JOIN meter.hvod"+mmgg+" AS hvod ON hvod.ln = gis.ln";
                    "LEFT JOIN hv"+mmgg+" AS hv
            PreparedStatement pstat = conn.prepareStatement(sql);
            return pstat.executeQuery();
        } catch (Exception e) {
            System.err.println("Error occurred in getGisAll");
            e.printStackTrace();
            return null;
        }
    }
*/

}
