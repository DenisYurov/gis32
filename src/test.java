import database.database;


import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

import static database.database.dbfConnection;
import static database.database.updateSQL;


/**
 * Created by dionis on 13.10.2016.
 */
public class test {
    public static void main(String[] args) {
        try {
            Properties prop = readConfig("config.properties");
            database.prop = prop;
            //accgilcomerrDAO.insertBadAccount();
            Connection conn = dbfConnection();
            updateSQL(conn, "PACK orgcp");
        } catch (Exception e) {
            System.err.println("Error occurred while read config file");
            e.printStackTrace();
            System.exit(4);
        }
    }

    // чтение конфигурационного файла
    private static Properties readConfig(String path) throws Exception {
        InputStream inputStream = Main.class.getResourceAsStream(path);
        Properties props = new Properties();
        props.load(inputStream);
        return props;
    }
}
