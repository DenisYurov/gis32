package ui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

/**
 * Created by dionis on 20.11.2016.
 */
public class rootLayoutController {
    //@FXML
    //private AnchorPane centerPane;
    @FXML
    private BorderPane mainPane;

    @FXML
    private void handleBill() throws IOException {
        Node node;
        node = (Node) FXMLLoader.load(getClass().getResource("bill.fxml"));
        mainPane.setCenter(node);
        //mainPane.getChildren().setAll(node);
    }
    @FXML
    private void handleGetBill() throws IOException {
        Node node;
        node = (Node) FXMLLoader.load(getClass().getResource("gilcomaccerr.fxml"));
        mainPane.setCenter(node);
        //mainPane.getChildren().setAll(node);
    }

    @FXML
    private void handleNsiloc51() throws IOException {
        Node node;
        node = (Node) FXMLLoader.load(getClass().getResource("nsiloc51.fxml"));
        mainPane.setCenter(node);

    }
    @FXML
    private void handleNsiloc1() throws IOException {
        Node node;
        node = (Node) FXMLLoader.load(getClass().getResource("nsiloc1.fxml"));
        mainPane.setCenter(node);

    }
    @FXML
    private void handleNsiloc50() throws IOException {
        Node node;
        node = (Node) FXMLLoader.load(getClass().getResource("nsi50.fxml"));
        mainPane.setCenter(node);

    }
    @FXML
    private void handleOrg() throws IOException {
        Node node;
        node = (Node) FXMLLoader.load(getClass().getResource("org.fxml"));
        mainPane.setCenter(node);

    }
    @FXML
    private void handleHouse() throws IOException {
        Node node;
        node = (Node) FXMLLoader.load(getClass().getResource("house.fxml"));
        mainPane.setCenter(node);

    }
    @FXML
    private void handleAccount() throws IOException {
        Node node;
        node = (Node) FXMLLoader.load(getClass().getResource("account.fxml"));
        mainPane.setCenter(node);

    }
}
