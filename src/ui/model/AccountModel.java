package ui.model;

import database.database;
import javafx.beans.property.SimpleStringProperty;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by dionis on 29.11.2016.
 */
public class AccountModel {
    private final SimpleStringProperty name;
    private final SimpleStringProperty orgppaguid;
    private final SimpleStringProperty accountnum;
    private final SimpleStringProperty unifiedacc;
    private final SimpleStringProperty serviceid;

    public AccountModel(String one, String two, String three,String four, String five) {
        this.name = new SimpleStringProperty(one);
        this.orgppaguid = new SimpleStringProperty(two);
        this.accountnum = new SimpleStringProperty(three);
        this.unifiedacc = new SimpleStringProperty(four);
        this.serviceid = new SimpleStringProperty(five);
    }

    public String getName() { return name.get(); }
    public String getOrgppaguid() { return orgppaguid.get();}
    public String getAccountnum() { return accountnum.get();}
    public String getUnifiedacc() { return unifiedacc.get();}
    public String getServiceid() { return serviceid.get();}
}
