package ui.model;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by dionis on 18.11.2016.
 */
public  class BillModel {
        private final SimpleStringProperty billId;
        private final SimpleStringProperty accoGuid;
        private final SimpleStringProperty error;
        private final SimpleStringProperty errorSoap;

        public BillModel(String fName, String lName, String error, String errorSoap) {
            this.billId = new SimpleStringProperty(fName);
            this.accoGuid = new SimpleStringProperty(lName);
            this.error = new SimpleStringProperty(error);
            this.errorSoap = new SimpleStringProperty(errorSoap);
        }
        public String getBillId() {
            return billId.get();
        }
        public String getAccoGuid() {
            return accoGuid.get();
        }
        public String getError() {
            return error.get();
        }
        public String getErrorSoap() {
        return errorSoap.get();
    }
}
