package ui.model;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by dionis on 25.11.2016.
 */
public class OrgModel {
    private final SimpleStringProperty ogrn;
    private final SimpleStringProperty orgppaguid;
    private final SimpleStringProperty name;

    public OrgModel(String one, String two, String three) {
        this.ogrn = new SimpleStringProperty(one);
        this.orgppaguid = new SimpleStringProperty(two);
        this.name = new SimpleStringProperty(three);
    }

    public String getOgrn() {
        return ogrn.get();
    }
    public String getOrgppaguid() { return orgppaguid.get();}
    public String getName() {
        return name.get();
    }
}
