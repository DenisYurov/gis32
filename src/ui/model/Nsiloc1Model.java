package ui.model;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by dionis on 21.11.2016.
 */
public class Nsiloc1Model {
    private final SimpleStringProperty code;
    private final SimpleStringProperty Guid;
    private final SimpleStringProperty orgppaguid;
    private final SimpleStringProperty value;
    private final SimpleStringProperty name;

    public Nsiloc1Model(String one, String two, String three,String four,String five) {
        this.code = new SimpleStringProperty(one);
        this.Guid = new SimpleStringProperty(two);
        this.orgppaguid = new SimpleStringProperty(three);
        this.value = new SimpleStringProperty(four);
        this.name = new SimpleStringProperty(five);
    }

    public String getCode() {
        return code.get();
    }
    public String getGuid() {
        return Guid.get();
    }
    public String getOrgppaguid() { return orgppaguid.get();}
    public String getValue() {
        return value.get();
    }
    public String getName() {
        return name.get();
    }
}
