package ui.model;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by dionis on 12.12.2016.
 */
public class Nsi50Model {
    private final SimpleStringProperty code;
    private final SimpleStringProperty Guid;
    private final SimpleStringProperty value;

    public Nsi50Model(String one, String two, String three) {
        this.code = new SimpleStringProperty(one);
        this.Guid = new SimpleStringProperty(two);
        this.value = new SimpleStringProperty(three);
    }

    public String getCode() {
        return code.get();
    }
    public String getGuid() {
        return Guid.get();
    }
    public String getValue() {
        return value.get();
    }
}
