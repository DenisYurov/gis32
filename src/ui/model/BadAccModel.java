package ui.model;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by dionis on 07.12.2016.
 */
public class BadAccModel {
        private final SimpleStringProperty ln;
        private final SimpleStringProperty error;

        public BadAccModel(String ln, String error) {
            this.ln = new SimpleStringProperty(ln);
            this.error = new SimpleStringProperty(error);
        }
        public String getLn() {
            return ln.get();
        }
        public String getError() {
            return error.get();
        }
}
