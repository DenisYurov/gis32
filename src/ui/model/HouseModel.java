package ui.model;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by dionis on 29.11.2016.
 */
public class HouseModel {
    private final SimpleStringProperty name;
    private final SimpleStringProperty fiasguid;
    private final SimpleStringProperty address;

    public HouseModel(String one, String two, String three) {
        this.name = new SimpleStringProperty(one);
        this.fiasguid = new SimpleStringProperty(two);
        this.address = new SimpleStringProperty(three);
    }

    public String getName() {
        return name.get();
    }
    public String getFiasguid() { return fiasguid.get();}
    public String getAddress() { return address.get();}
}
