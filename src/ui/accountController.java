package ui;

import dataobject.accountDAO;
import dataobject.houseDAO;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import ui.model.AccountModel;

import java.sql.ResultSet;

import static jaxws.accountrequest.exportAccountRequest;

/**
 * Created by dionis on 29.11.2016.
 */
public class accountController {
    @FXML
    private TableView<AccountModel> accountTable;
    @FXML
    private TableColumn<AccountModel, String> colOrgName;
    @FXML
    private TableColumn<AccountModel, String> colOrgPpaGuid;
    @FXML
    private TableColumn<AccountModel, String> colAccNum;
    @FXML
    private TableColumn<AccountModel, String> colUniAccNum;
    @FXML
    private TableColumn<AccountModel, String> colServiceID;
    @FXML
    private TextField filterField;

    @FXML
    private ProgressBar progressBar;

    @FXML
    void initialize() {
        tableLoadData();
        FilteredList<AccountModel> filteredData = new FilteredList<>(data, p -> true);
        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(account -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();
                /*
                if (account.getAccountnum().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (account.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                return false; // Does not match.
                */
                if (account.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                if (account.getAccountnum().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches Accountnum.
                } else if (account.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches name.
                }
                if (account.getServiceid().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                if (account.getUnifiedacc().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                if (account.getOrgppaguid().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;

            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<AccountModel> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(accountTable.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        accountTable.setItems(sortedData);

    }

    private void tableLoadData() {
        assert accountTable != null : "fx:id=\"accountTable\" was not injected: check your FXML file 'house.fxml'.";
        colOrgName.setCellValueFactory(
                new PropertyValueFactory<AccountModel, String>("name"));
        colOrgPpaGuid.setCellValueFactory(
                new PropertyValueFactory<AccountModel, String>("orgppaguid"));
        colAccNum.setCellValueFactory(
                new PropertyValueFactory<AccountModel, String>("accountnum"));
        colUniAccNum.setCellValueFactory(
                new PropertyValueFactory<AccountModel, String>("unifiedacc"));
        colServiceID.setCellValueFactory(
                new PropertyValueFactory<AccountModel, String>("serviceid"));

        buildData();
    }

    private ObservableList<AccountModel> data;

    //чтение данных из таблицы и запись их в ObservableList
    public void buildData() {
        data = FXCollections.observableArrayList();
        try {
            ResultSet rs = accountDAO.getAccount();
            while (rs.next()) {
                AccountModel house = new AccountModel(rs.getString("name"), rs.getString("orgppaguid"), rs.getString("accountnum"), rs.getString("unifiedacc"), rs.getString("serviceid"));
                data.add(house);
            }
            accountTable.setItems(data);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }

    public static volatile SimpleDoubleProperty progress;

    @FXML
    private void handleHouseLoad() {
        class orgThread extends Thread {
            @Override
            public void run() {
                ResultSet rs = dataobject.houseDAO.getHouse();
                int houseCnt = dataobject.houseDAO.getHouseCount();
                double progressDelta = (double)1/houseCnt;
                double progressCnt = 0;
                try {
                    while (rs.next()) {
                        exportAccountRequest(rs.getString("orgppaguid"), rs.getString("fiasguid"));
                        progressCnt = progressCnt + progressDelta;
                        progress.set(progressCnt);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                    System.out.println("Error on handleHouseLoad");
                }
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tableLoadData();
                    }
                });
            }
        }
        progress = new SimpleDoubleProperty(0);
        progressBar.progressProperty().bind(progress);
        Thread orgThread = new orgThread();
        orgThread.start();
    }
}
