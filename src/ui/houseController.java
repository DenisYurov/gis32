package ui;

import dataobject.houseDAO;
import dataobject.orgDAO;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import jaxws.nsi;
import ui.model.AccountModel;
import ui.model.HouseModel;

import java.sql.ResultSet;

import static jaxws.accountrequest.exportAccountRequest;

/**
 * Created by dionis on 29.11.2016.
 */
public class houseController {
    @FXML
    private TableView<HouseModel> houseTable;
    @FXML
    private TableColumn<HouseModel, String> colOrgName;
    @FXML
    private TableColumn<HouseModel, String> colFias;
    @FXML
    private TableColumn<HouseModel, String> colAddress;
    @FXML
    private TextField filterField;
    @FXML
    private ProgressBar progressBar;


    @FXML
    void initialize() {
        tableLoadData();
        FilteredList<HouseModel> filteredData = new FilteredList<>(data, p -> true);
        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(house -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (house.getFiasguid().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches Accountnum.
                } else if (house.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches name.
                }
                if (house.getAddress().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }

                return false;// Does not match.

            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<HouseModel> sortedData = new SortedList<>(filteredData);
        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(houseTable.comparatorProperty());
        // 5. Add sorted (and filtered) data to the table.
        houseTable.setItems(sortedData);
    }

    private void tableLoadData() {
        assert houseTable != null : "fx:id=\"houseTable\" was not injected: check your FXML file 'house.fxml'.";
        colOrgName.setCellValueFactory(
                new PropertyValueFactory<HouseModel, String>("name"));
        colFias.setCellValueFactory(
                new PropertyValueFactory<HouseModel, String>("fiasguid"));
        colAddress.setCellValueFactory(
                new PropertyValueFactory<HouseModel, String>("address"));
        buildData();
    }

    private ObservableList<HouseModel> data;

    //чтение данных из таблицы и запись их в ObservableList
    public void buildData() {
        data = FXCollections.observableArrayList();
        try {
            ResultSet rs = houseDAO.getHouse();
            while (rs.next()) {
                HouseModel house = new HouseModel(rs.getString("name"), rs.getString("fiasguid"), rs.getString("address"));
                data.add(house);
            }
            houseTable.setItems(data);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }

    public static volatile SimpleDoubleProperty progress;

    @FXML
    private void handleHouseLoad() {
        //Создаем поток
        class orgThread extends Thread {
            @Override
            public void run() {
                dataobject.houseDAO.insertGilcomHouse();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tableLoadData();
                    }
                });
            }
        }
        /*
        nsi.progress = new SimpleDoubleProperty(0);
        progressBar.progressProperty().bind(nsi.progress);
        */
        houseDAO.progress = new SimpleDoubleProperty(0);
        progressBar.progressProperty().bind(houseDAO.progress);
        Thread orgThread = new orgThread();
        orgThread.start();

    }
}
