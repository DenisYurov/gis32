package ui;

import database.gilcomtogisdbf;
import database.gilcomtogisdbfcheck;
import dataobject.billdocDAO;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import jaxws.billsservice;
import ui.model.BadAccModel;
import ui.model.BadAccModel;

import java.sql.ResultSet;

import static dataobject.accgilcomerrDAO.getAccGilcomErr;

/**
 * Created by dionis on 07.12.2016.
 */
public class gilcomaccerrController {
    @FXML
    private TableView<BadAccModel> accountTable;
    @FXML
    private TableColumn<BadAccModel, String> colAccNum;
    @FXML
    private TableColumn<BadAccModel, String> colError;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private TextField filterField;
    //инициализация контроллера
    @FXML
    void initialize(){
        tableLoadData();
        inicialiseSearch();
        
    }
    private void inicialiseSearch(){
        FilteredList<BadAccModel> filteredData = new FilteredList<>(data, p -> true);
        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(account -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (account.getLn().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter ln.
                } else if (account.getError().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter error.
                }

                return false;

            });
        });
        // 3. Wrap the FilteredList in a SortedList.
        SortedList<BadAccModel> sortedData = new SortedList<>(filteredData);
        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(accountTable.comparatorProperty());
        // 5. Add sorted (and filtered) data to the table.
        accountTable.setItems(sortedData);

    }
    private void tableLoadData() {
        assert accountTable != null : "fx:id=\"accountTable\" was not injected: check your FXML file 'gilcomaccerr.fxml'.";
        colAccNum.setCellValueFactory(
                new PropertyValueFactory<BadAccModel,String>("ln"));
        colError.setCellValueFactory(
                new PropertyValueFactory<BadAccModel,String>("error"));

        buildData();
    }
    private ObservableList<BadAccModel> data;
    //чтение данных из таблицы и запись их в ObservableList
    public void buildData(){
        data = FXCollections.observableArrayList();
        try{
            //получаем все начисления из базы данных
            ResultSet rs = getAccGilcomErr();
            while(rs.next()){
                BadAccModel badAcc = new BadAccModel(rs.getString("ln"),rs.getString("error"));
                data.add(badAcc);
            }
            accountTable.setItems(data);
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }
    @FXML
    private void handleDbfTDbf() {
        //Создаем поток
        class nsiThread extends Thread {
            @Override
            public void run() {
                try {
                    gilcomtogisdbf.moveData();
                }catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error on gilcomtogisdbf.moveData()");
                }
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tableLoadData();
                    }
                });
            }
        }
        gilcomtogisdbf.progress = new SimpleDoubleProperty(0);
        progressBar.progressProperty().bind(gilcomtogisdbf.progress);
        Thread nsiThread = new nsiThread();
        nsiThread.start();


    }
    @FXML
    private void handleCheckBadAcc() {
        //Создаем поток
        class nsiThread extends Thread {
            @Override
            public void run() {
                gilcomtogisdbfcheck.findtBadAccount();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tableLoadData();
                    }
                });
            }
        }
        gilcomtogisdbfcheck.progress = new SimpleDoubleProperty(0);
        progressBar.progressProperty().bind(gilcomtogisdbfcheck.progress);
        Thread nsiThread = new nsiThread();
        nsiThread.start();


    }
}
