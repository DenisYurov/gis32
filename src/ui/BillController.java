package ui;

import database.gilcomtogisdbf;
import dataobject.billdocDAO;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import jaxws.billsservice;
import jaxws.nsi;
import ui.model.BillModel;

import java.sql.ResultSet;

import static dataobject.billsoaplogDAO.getBillSoapLog;

public class BillController {
    //переменные видимые в fxml
    @FXML
    private TableView<BillModel> billTable;
    @FXML
    private TableColumn<BillModel, String> colBillId;
    @FXML
    private TableColumn<BillModel, String> colAccoGuid;
    @FXML
    private TableColumn<BillModel, String> colError;
    @FXML
    private TableColumn<BillModel, String> colErrorSoap;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private TextField filterField;

    //инициализация контроллера
    @FXML
    void initialize() {
        tableLoadData();
        inicialiseSearch();
    }

    private void tableLoadData() {
        assert billTable != null : "fx:id=\"billTable\" was not injected: check your FXML file 'bill.fxml'.";
        colBillId.setCellValueFactory(
                new PropertyValueFactory<BillModel, String>("billId"));
        colAccoGuid.setCellValueFactory(
                new PropertyValueFactory<BillModel, String>("accoGuid"));
        colError.setCellValueFactory(
                new PropertyValueFactory<BillModel, String>("error"));
        colErrorSoap.setCellValueFactory(
                new PropertyValueFactory<BillModel, String>("errorSoap"));

        buildData();
        //getTableView().getItems().size() число строк
    }

    private void inicialiseSearch() {
        FilteredList<BillModel> filteredData = new FilteredList<>(data, p -> true);
        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(bill -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();
                if (bill.getAccoGuid().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });
        // 3. Wrap the FilteredList in a SortedList.
        SortedList<BillModel> sortedData = new SortedList<>(filteredData);
        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(billTable.comparatorProperty());
        // 5. Add sorted (and filtered) data to the table.
        billTable.setItems(sortedData);

    }

    private ObservableList<BillModel> data;

    //чтение данных из таблицы и запись их в ObservableList
    public void buildData() {
        data = FXCollections.observableArrayList();
        try {
            //получаем все начисления из базы данных
            ResultSet rs = billdocDAO.getAllBill();
            while (rs.next()) {
                //извлекаем ошибки SOAP
                ResultSet billSoapRS = getBillSoapLog(rs.getString("orgppaguid"));
                String billSoap = "";
                if (billSoapRS.next()){
                    billSoap = billSoapRS.getString("error");
                }
                //System.out.println("1: "+billSoap+" "+rs.getString("orgppaguid"));
                BillModel bill = new BillModel(rs.getString("billid"), rs.getString("accoguid"), rs.getString("error"),billSoap);
                data.add(bill);
            }
            billTable.setItems(data);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }

    @FXML
    private void handleStart() {
        //Создаем поток
        class nsiThread extends Thread {
            @Override
            public void run() {
                jaxws.billsservice.allBillSend();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tableLoadData();
                    }
                });
            }
        }
        billsservice.progress = new SimpleDoubleProperty(0);
        progressBar.progressProperty().bind(billsservice.progress);
        Thread nsiThread = new nsiThread();
        nsiThread.start();


    }
/*
    @FXML
    private void handleDbfTDbf() {
        //Создаем поток
        class nsiThread extends Thread {
            @Override
            public void run() {
                try {
                    gilcomtogisdbf.moveData();
                }catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error on gilcomtogisdbf.moveData()");
                }
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tableLoadData();
                    }
                });
            }
        }
        gilcomtogisdbf.progress = new SimpleDoubleProperty(0);
        progressBar.progressProperty().bind(gilcomtogisdbf.progress);
        Thread nsiThread = new nsiThread();
        nsiThread.start();


    }
    */
}
