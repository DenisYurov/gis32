package ui;

import dataobject.orgDAO;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.json.JSONObject;
import ui.model.OrgModel;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.ResultSet;

import static dataobject.orgDAO.getOrgRecCount;

/**
 * Created by dionis on 24.11.2016.
 */
public class orgController {
    //переменные видимые в fxml
    @FXML
    private TableView<OrgModel> orgTable;
    @FXML
    private TableColumn<OrgModel, String> colOrgName;
    @FXML
    private TableColumn<OrgModel, String> colOgrn;
    @FXML
    private TableColumn<OrgModel, String> colOrgPpaGuid;

    @FXML
    private ProgressBar progressBar;
    @FXML
    private CheckBox showDataProvider;
    @FXML
    private TextField filterField;

    @FXML
    void initialize() {
        tableLoadData();
        inicialiseSearch();
    }
    private void inicialiseSearch(){
        FilteredList<OrgModel> filteredData = new FilteredList<>(data, p -> true);
        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(org -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (org.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter Name.
                } else if (org.getOgrn().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter OGRN.
                }

                return false;

            });
        });
        // 3. Wrap the FilteredList in a SortedList.
        SortedList<OrgModel> sortedData = new SortedList<>(filteredData);
        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(orgTable.comparatorProperty());
        // 5. Add sorted (and filtered) data to the table.
        orgTable.setItems(sortedData);
    }

    private void tableLoadData() {
        assert orgTable != null : "fx:id=\"nsi1Table\" was not injected: check your FXML file 'org.fxml'.";
        colOgrn.setCellValueFactory(
                new PropertyValueFactory<OrgModel, String>("ogrn"));

        colOrgPpaGuid.setCellValueFactory(
                new PropertyValueFactory<OrgModel, String>("orgppaguid"));

        colOrgName.setCellValueFactory(
                new PropertyValueFactory<OrgModel, String>("name"));

        buildData();
    }

    private ObservableList<OrgModel> data;

    //чтение данных из таблицы и запись их в ObservableList
    public void buildData() {
        data = FXCollections.observableArrayList();
        try {
            ResultSet rs = orgDAO.getAllOrg(showDataProvider.isSelected());
            while (rs.next()) {
                OrgModel org = new OrgModel(rs.getString("ogrn"), rs.getString("orgppaguid"), rs.getString("name"));
                data.add(org);
            }
            orgTable.setItems(data);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }

public static volatile SimpleDoubleProperty progress;

    @FXML
    private void handleOrgLoad() {

        //Создаем поток

        class orgThread extends Thread {
            @Override
            public void run() {
                int orgNoGuidRecCount = dataobject.orgDAO.getOrgNoGuidRecCount();
                if (orgNoGuidRecCount == 0){progress.set(1);return;}
                double progressDelta = (double)1/orgNoGuidRecCount;
                double progressCnt = 0;
                ResultSet org = dataobject.orgDAO.getOrgnoGuid();
                try {
                    while(org.next()){
                        jaxws.regorg.exportOrgRegistryRequest(org.getString("ogrn"));
                        progressCnt = progressCnt + progressDelta;
                        progress.set(progressCnt);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error on handleOrgLoad");
                }
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tableLoadData();
                    }
                });
            }
        }
        progress = new SimpleDoubleProperty(0);
        progressBar.progressProperty().bind(progress);
        Thread orgThread = new orgThread();
        orgThread.start();

    }
    @FXML
    private void handleShowDataProvider(){
        tableLoadData();
        inicialiseSearch();
    }
    /*
    @FXML
    private void handleTest(){
        //dataobject.orgDAO.getTest();
        dataobject.houseDAO.insertGilcomHouse();
    }
    */
    // HTTP POST request не работает
    private void sendPost() throws Exception {
        String USER_AGENT = "Mozilla/5.0";
        String url = "http://127.0.0.1:8080/ppa/api/rest/services/access/rights/providers;pageIndex=1;elementsPerPage=10";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
        con.setRequestProperty("Content-Type","application/json;charset=UTF-8");
        con.setRequestProperty("Accept","application/json;charset=utf-8");
        /*Authorization:Basic c2l0OnJaX0dHNzJYU15WZjU1Wlc=
        Accept:application/json; charset=utf-8
        Accept-Encoding:gzip, deflate
        Accept-Language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4
        Authorization:Basic c2l0OnJaX0dHNzJYU15WZjU1Wlc=
                Connection:keep-alive
        Content-Length:144
        Content-Type:application/json;charset=UTF-8
                */
/*{"assigneeOrganizationGuid":"8449732e-f377-40e4-b3f4-915d30268c6f","sortCriteria":{"sortedBy":"assignerOrganization.fullName","ascending":true}}*/
        JSONObject ass = new JSONObject();
        JSONObject auth=new JSONObject();
        JSONObject parent=new JSONObject();
        ass.put("assigneeOrganizationGuid","8449732e-f377-40e4-b3f4-915d30268c6f");


        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(ass.toString());
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

    }
}
