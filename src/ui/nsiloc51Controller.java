package ui;

import dataobject.nsi51DAO;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import jaxws.nsi;
import ui.model.Nsiloc51Model;

import java.sql.ResultSet;

/**
 * Created by dionis on 20.11.2016.
 */
public class nsiloc51Controller {
    //переменные видимые в fxml
    @FXML
    private TableView<Nsiloc51Model> nsi51Table;
    @FXML
    private TableColumn<Nsiloc51Model, String> colCode;
    @FXML
    private TableColumn<Nsiloc51Model, String> colGuid;
    @FXML
    private TableColumn<Nsiloc51Model, String> colOrgPpaGuid;
    @FXML
    private TableColumn<Nsiloc51Model, String> colValue;
    @FXML
    private TableColumn<Nsiloc51Model, String> colOrgName;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private TextField filterField;
    @FXML
    private CheckBox checkDataProvider;

    @FXML
    void initialize() {
        tableLoadData();
        inicialiseSearch();
    }

    private void inicialiseSearch(){
        FilteredList<Nsiloc51Model> filteredData = new FilteredList<>(data, p -> true);
        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(nsiItem -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (nsiItem.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter Name.
                } else if (nsiItem.getCode().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter OGRN.
                }
                if (nsiItem.getGuid().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                if (nsiItem.getOrgppaguid().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                if (nsiItem.getValue().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;

            });
        });
        // 3. Wrap the FilteredList in a SortedList.
        SortedList<Nsiloc51Model> sortedData = new SortedList<>(filteredData);
        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(nsi51Table.comparatorProperty());
        // 5. Add sorted (and filtered) data to the table.
        nsi51Table.setItems(sortedData);
    }


    private void tableLoadData() {
        assert nsi51Table != null : "fx:id=\"nsi1Table\" was not injected: check your FXML file 'nsiloc1.fxml'.";
        colCode.setCellValueFactory(
                new PropertyValueFactory<Nsiloc51Model, String>("code"));
        colGuid.setCellValueFactory(
                new PropertyValueFactory<Nsiloc51Model, String>("guid"));
        colOrgPpaGuid.setCellValueFactory(
                new PropertyValueFactory<Nsiloc51Model, String>("orgppaguid"));
        colValue.setCellValueFactory(
                new PropertyValueFactory<Nsiloc51Model, String>("value"));
        colOrgName.setCellValueFactory(
                new PropertyValueFactory<Nsiloc51Model, String>("name"));

        buildData();
    }

    private ObservableList<Nsiloc51Model> data;

    //чтение данных из таблицы и запись их в ObservableList
    public void buildData() {
        data = FXCollections.observableArrayList();
        try {

            ResultSet rs = nsi51DAO.getNsi51AllItem();
            while (rs.next()) {
                Nsiloc51Model bill = new Nsiloc51Model(rs.getString("code"), rs.getString("guid"), rs.getString("orgppaguid"), rs.getString("value"), rs.getString("name"));
                data.add(bill);
            }
            nsi51Table.setItems(data);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }

    @FXML
    private void handleGetNsi51() {
        //Создаем поток
        class nsiThread extends Thread {
            @Override
            public void run() {
                nsi.exportDataProviderNsiItemRequestAllOrg("51", !checkDataProvider.isSelected());
                //после завершения потока
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tableLoadData();
                    }
                });
            }
        }
        nsi.progress = new SimpleDoubleProperty(0);
        progressBar.progressProperty().bind(nsi.progress);
        Thread n = new nsiThread();
        n.start();
    }

}
