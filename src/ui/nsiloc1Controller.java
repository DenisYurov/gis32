package ui;

import dataobject.nsi1DAO;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import jaxws.nsi;
import ui.model.Nsiloc1Model;

import java.sql.ResultSet;

/**
 * Created by dionis on 21.11.2016.
 */
public class nsiloc1Controller {

    //переменные видимые в fxml
    @FXML
    private TableView<Nsiloc1Model> nsi1Table;
    @FXML
    private TableColumn<Nsiloc1Model, String> colCode;
    @FXML
    private TableColumn<Nsiloc1Model, String> colGuid;
    @FXML
    private TableColumn<Nsiloc1Model, String> colOrgPpaGuid;
    @FXML
    private TableColumn<Nsiloc1Model, String> colValue;
    @FXML
    private TableColumn<Nsiloc1Model, String> colOrgName;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private CheckBox checkDataProvider;
    @FXML
    private TextField filterField;

    @FXML
    void initialize() {
        tableLoadData();
        inicialiseSearch();
    }

    private void inicialiseSearch() {
        FilteredList<Nsiloc1Model> filteredData = new FilteredList<>(data, p -> true);
        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(nsiItem -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (nsiItem.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter NAme.
                } else if (nsiItem.getValue().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter Value.
                }
                if (nsiItem.getCode().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                if (nsiItem.getOrgppaguid().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }

                return false;

            });
        });
        // 3. Wrap the FilteredList in a SortedList.
        SortedList<Nsiloc1Model> sortedData = new SortedList<>(filteredData);
        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(nsi1Table.comparatorProperty());
        // 5. Add sorted (and filtered) data to the table.
        nsi1Table.setItems(sortedData);

    }

    private void tableLoadData() {
        assert nsi1Table != null : "fx:id=\"nsi1Table\" was not injected: check your FXML file 'nsiloc1.fxml'.";
        colCode.setCellValueFactory(
                new PropertyValueFactory<Nsiloc1Model, String>("code"));
        colGuid.setCellValueFactory(
                new PropertyValueFactory<Nsiloc1Model, String>("guid"));
        colOrgPpaGuid.setCellValueFactory(
                new PropertyValueFactory<Nsiloc1Model, String>("orgppaguid"));
        colValue.setCellValueFactory(
                new PropertyValueFactory<Nsiloc1Model, String>("value"));
        colOrgName.setCellValueFactory(
                new PropertyValueFactory<Nsiloc1Model, String>("name"));

        buildData();
    }

    private ObservableList<Nsiloc1Model> data;

    //чтение данных из таблицы и запись их в ObservableList
    public void buildData() {
        data = FXCollections.observableArrayList();
        try {

            ResultSet rs = nsi1DAO.getNsi1AllItem();
            while (rs.next()) {
                Nsiloc1Model bill = new Nsiloc1Model(rs.getString("code"), rs.getString("guid"), rs.getString("orgppaguid"), rs.getString("value"), rs.getString("name"));
                data.add(bill);
            }
            nsi1Table.setItems(data);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }

    // private Thread nsiThread;
    /*
    @FXML
    private Label duLabel;
*/
    @FXML
    private void handleGetNsi1() {
        //Создаем поток
        class nsiThread extends Thread {
            @Override
            public void run() {
                nsi.exportDataProviderNsiItemRequestAllOrg("1", !checkDataProvider.isSelected());
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tableLoadData();
                    }
                });
            }
        }
        nsi.progress = new SimpleDoubleProperty(0);
        progressBar.progressProperty().bind(nsi.progress);
        Thread nsiThread = new nsiThread();
        nsiThread.start();

    }

}
