package ui;

import dataobject.nsi50DAO;
import dataobject.nsi51DAO;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import jaxws.nsi;
import jaxws.nsi_common;
import ui.model.Nsi50Model;
import ui.model.Nsiloc51Model;

import java.sql.ResultSet;

/**
 * Created by dionis on 12.12.2016.
 */
public class nsi50Controller {
    //переменные видимые в fxml
    @FXML
    private TableView<Nsi50Model> nsi50Table;
    @FXML
    private TableColumn<Nsi50Model, String> colCode;
    @FXML
    private TableColumn<Nsi50Model, String> colGuid;
    @FXML
    private TableColumn<Nsi50Model, String> colValue;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private TextField filterField;

    @FXML
    void initialize() {
        tableLoadData();
        //inicialiseSearch();
    }
    private void tableLoadData() {
        assert nsi50Table != null : "fx:id=\"nsi1Table\" was not injected: check your FXML file 'nsi50.fxml'.";
        colCode.setCellValueFactory(
                new PropertyValueFactory<Nsi50Model, String>("code"));
        colGuid.setCellValueFactory(
                new PropertyValueFactory<Nsi50Model, String>("guid"));

        colValue.setCellValueFactory(
                new PropertyValueFactory<Nsi50Model, String>("value"));

        buildData();
    }
    private ObservableList<Nsi50Model> data;

    //чтение данных из таблицы и запись их в ObservableList
    public void buildData() {
        data = FXCollections.observableArrayList();
        try {

            ResultSet rs = nsi50DAO.getNsi50AllItem();
            while (rs.next()) {
                Nsi50Model bill = new Nsi50Model(rs.getString("code"), rs.getString("guid"), rs.getString("value"));
                data.add(bill);
            }
            nsi50Table.setItems(data);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }
    @FXML
    private void handleGetNsi50() {
        //Создаем поток
        class nsiThread extends Thread {
            @Override
            public void run() {
                nsi_common.ExportNsiItemRequestF("50","NSI");
                //после завершения потока
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tableLoadData();
                    }
                });
            }
        }
        nsi.progress = new SimpleDoubleProperty(0);
        progressBar.progressProperty().bind(nsi.progress);
        Thread n = new nsiThread();
        n.start();
    }

}
