import java.io.*;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.*;

import database.database;
import database.gilcomtogisdbf;
import dataobject.jilservDAO;
import dataobject.liceDAO;
import dataobject.municservDAO;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import jaxws.*;
import soap.queries.*;

import signature.sign;

import static database.database.getGGGGNow;
import static database.database.getMMNow;

public class Main extends Application {

    /**
     *
     */
    public static Properties prop;

    @Override
    public void start(Stage primaryStage) throws Exception {
        //Parent root = FXMLLoader.load(getClass().getResource("ui/bill.fxml"));
        Parent root = FXMLLoader.load(getClass().getResource("ui/rootlayout.fxml"));
        String src = "ГиС ЖКХ";
        primaryStage.setTitle(new String(src.getBytes("Cp1251"), "UTF-8"));
        primaryStage.setScene(new Scene(root, 1260, 640));
        primaryStage.show();
    }

    @Override
    public void stop() {
        try {
            if (database.conn != null &&!database.conn.isClosed()) {
                database.conn.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            //передаем конфигурационный файл в модули
            prop = readConfig("config.properties");
            database.prop = prop;
            fictionresponse.prop = prop;
            utility.prop = prop;
            exportAccountRequest.prop = prop;
            devicemetering.prop = prop;
            billsservice.prop = prop;
            sign.prop = prop;
            accountrequest.prop = prop;
            regorg.prop = prop;
            nsi.prop = prop;
            liceDAO.prop = prop;
            jilservDAO.prop = prop;
            municservDAO.prop = prop;
            gilcomtogisdbf.prop = prop;
            nsi_common.prop =prop;
            //добавляем basic-авторизацию ко всем http запросам
            Authenticator.setDefault(new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(
                            prop.getProperty("USER_NAME"),
                            prop.getProperty("PASSWORD").toCharArray());
                }
            });

        } catch (Exception e) {
            System.err.println("Error occurred while read config file");
            e.printStackTrace();
            System.exit(4);
        }

        launch(args);
    }


    public static void xmain(String args[]) {


        /* тут вызов запросов
        * технология SAAJ - SOAP Client
        */
        //ExportNsiList.runQuery();
        //accountrequest.exportAccountRequest("c1eb5b30-9f4b-4e12-b843-1090ea3e25a4","7b1d42dc-ad93-4437-ade5-3b81cd523b46");
        //accountrequest.exportAccountRequest("839e8b55-0ac1-452b-b7ad-b2ed12fbcd03","ebd344b4-47ba-44ff-a8da-5e8f328745b8");
        //ExportMeteringDeviceDataRequest("7b1d42dc-ad93-4437-ade5-3b81cd523b46","1","3a9687b5-caed-4ec6-8a08-f4d3d012f2c7",null);
        //ExportNsiItemRequestF("50","NSI");
        //exportDataProviderNsiItemRequest("839e8b55-0ac1-452b-b7ad-b2ed12fbcd03","1");
        //device guid 5b30b1c7-a83b-4162-a8ac-a119b3a628b0
        //ImportMeteringDeviceValuesRequestElectric("7b1d42dc-ad93-4437-ade5-3b81cd523b46","aa1d8057-142b-4927-a9bc-72894f0b3c36", new BigDecimal("13.0002"),new BigDecimal("13.0200"));
        //ImportMeteringDeviceValuesRequestOneRate("7b1d42dc-ad93-4437-ade5-3b81cd523b46","9ed866c6-5b74-4507-9a1b-c96c372f1e76","1","c93bb0cd-0964-4253-a42a-4115130f4cab",new BigDecimal("9.5000"));
        //importPaymentDocumentData("c1eb5b30-9f4b-4e12-b843-1090ea3e25a4");
        //regorg.exportOrgRegistryRequest("1085321006583");
        //regorg.exportDataProvider();

        // уют ОГРН 1055301030355 orgPPAGUID 839e8b55-0ac1-452b-b7ad-b2ed12fbcd03
        //МУП ИАЦ ОГРН 1085321006583 orgPPAGUID 46eaec66-50ac-41ce-b70f-a7a86c406e61
        //тсж 7 сит1 ОГРН 1085321006583
        //тсж 7 сит1 ogrppaguid c1eb5b30-9f4b-4e12-b843-1090ea3e25a4
        //кочетова 10 к 1 FIAS 6637276c-81d4-4a1f-9adb-d0a45ebc50a1
        //кочетова 10 к 2 FIAS 7b1d42dc-ad93-4437-ade5-3b81cd523b46
        //кочетова 41 FIAS ebd344b4-47ba-44ff-a8da-5e8f328745b8
        //кочетова 41 кв 1 accountGuid 9b412f95-6bba-46c2-8da1-9bf0872d985b
        //FIASGUID sit1 "5b30b1c7-a83b-4162-a8ac-a119b3a628b0"
        //FIASGUID sit2 7b1d42dc-ad93-4437-ade5-3b81cd523b46
        //Municipal Resource холодная вода code 1 guid c93bb0cd-0964-4253-a42a-4115130f4cab
        //sit2 холодная вода rootguid 9ed866c6-5b74-4507-9a1b-c96c372f1e76
    }

    // чтение конфигурационного файла
    private static Properties readConfig(String path) throws Exception {
        InputStream inputStream = Main.class.getResourceAsStream(path);
        Properties props = new Properties();
        props.load(inputStream);
        return props;
    }

}
//http://127.0.0.1:8080/ext-bus-home-management-service/services/HomeManagement
/*
  xmlns = {
                @javax.xml.bind.annotation.XmlNs(
                        prefix = "hous",
                        namespaceURI = "http://dom.gosuslugi.ru/schema/integration/house-management/"),
                @javax.xml.bind.annotation.XmlNs(
                        prefix = "nsi",
                        namespaceURI = "http://dom.gosuslugi.ru/schema/integration/nsi-base/"),
                @javax.xml.bind.annotation.XmlNs(
                        prefix = "soapenv",
                        namespaceURI = "http://schemas.xmlsoap.org/soap/envelope/"),
                @javax.xml.bind.annotation.XmlNs(
                        prefix = "base",
                        namespaceURI = "http://dom.gosuslugi.ru/schema/integration/base/"),
        })
*/
