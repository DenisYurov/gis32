package jaxws;

import database.database;
import ru.gosuslugi.dom.schema.integration.base.CommonResultType;
import ru.gosuslugi.dom.schema.integration.base.HeaderType;
import ru.gosuslugi.dom.schema.integration.base.ImportResult;
import ru.gosuslugi.dom.schema.integration.base.RequestHeader;
import ru.gosuslugi.dom.schema.integration.device_metering.ImportMeteringDeviceValuesRequest;
import ru.gosuslugi.dom.schema.integration.device_metering_service.DeviceMeteringPortTypes;
import ru.gosuslugi.dom.schema.integration.house_management.*;
import ru.gosuslugi.dom.schema.integration.house_management_service.HouseManagementPortsType;
import ru.gosuslugi.dom.schema.integration.house_management_service.HouseManagementService;
import ru.gosuslugi.dom.schema.integration.nsi_base.NsiRef;
import ru.gosuslugi.dom.schema.integration.device_metering_service.DeviceMeteringService;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static database.database.recordExist;
import static jaxws.jaxutil.getGrigorian;
import static jaxws.jaxutil.logs;

/**
 * Created by dionis on 17.10.2016.
 */
public class devicemetering {
    public static Properties prop;
    //Отправка в гис показаний счетчиков

    //отправка счетчика электроэнергии
    public static void ImportMeteringDeviceValuesRequestElectric(String fIASHouseGuid,String meteringDeviceRootGUID,BigDecimal meteringValueT1,BigDecimal meteringValueT2){
        //формируем один из электросчетчиков. 1000 максимум
        ImportMeteringDeviceValuesRequest importMeteringDeviceValuesRequest = newDeviceRequestObj(fIASHouseGuid);
        ImportMeteringDeviceValuesRequest.MeteringDevicesValues meteringDevicesValues = new ImportMeteringDeviceValuesRequest.MeteringDevicesValues();
        meteringDevicesValues.setMeteringDeviceRootGUID(meteringDeviceRootGUID);
        ImportMeteringDeviceValuesRequest.MeteringDevicesValues.ElectricDeviceValue.CurrentValue cValue = new ImportMeteringDeviceValuesRequest.MeteringDevicesValues.ElectricDeviceValue.CurrentValue();
        cValue.setTransportGUID(java.util.UUID.randomUUID().toString());
        cValue.setMeteringValueT1(meteringValueT1);//показание1
        cValue.setMeteringValueT2(meteringValueT2);//показание2
        ImportMeteringDeviceValuesRequest.MeteringDevicesValues.ElectricDeviceValue eDV = new ImportMeteringDeviceValuesRequest.MeteringDevicesValues.ElectricDeviceValue();
        eDV.getCurrentValue().add(cValue);
        meteringDevicesValues.setElectricDeviceValue(eDV);
        //добавляем очередной счетчик
        importMeteringDeviceValuesRequest.getMeteringDevicesValues().add(meteringDevicesValues);
        ImportMeteringDeviceValuesRequestF(importMeteringDeviceValuesRequest);
    }
    //процедура отправки не электрического счетчика
    public static void ImportMeteringDeviceValuesRequestOneRate(String fIASHouseGuid,String meteringDeviceRootGUID,String municipalResourceC,String municipalResourceG,BigDecimal MeteringValue){
        //формируем один из не электрических счетчиков. 1000 максимум
        ImportMeteringDeviceValuesRequest importMeteringDeviceValuesRequest = newDeviceRequestObj(fIASHouseGuid);
        ImportMeteringDeviceValuesRequest.MeteringDevicesValues meteringDevicesValues = new ImportMeteringDeviceValuesRequest.MeteringDevicesValues();
        meteringDevicesValues.setMeteringDeviceRootGUID(meteringDeviceRootGUID);
        ImportMeteringDeviceValuesRequest.MeteringDevicesValues.OneRateDeviceValue.CurrentValue cValue = new ImportMeteringDeviceValuesRequest.MeteringDevicesValues.OneRateDeviceValue.CurrentValue();
        cValue.setTransportGUID(java.util.UUID.randomUUID().toString());
        NsiRef nsr = new NsiRef();
        nsr.setCode(municipalResourceC); //справочник 2
        nsr.setGUID(municipalResourceG);
        cValue.setMunicipalResource(nsr);
        cValue.setMeteringValue(MeteringValue); //показание
        ImportMeteringDeviceValuesRequest.MeteringDevicesValues.OneRateDeviceValue oRDeviceVal = new ImportMeteringDeviceValuesRequest.MeteringDevicesValues.OneRateDeviceValue();
        oRDeviceVal.getCurrentValue().add(cValue);
        meteringDevicesValues.setOneRateDeviceValue(oRDeviceVal);
        //добавляем очередной счетчик
        importMeteringDeviceValuesRequest.getMeteringDevicesValues().add(meteringDevicesValues);
        ImportMeteringDeviceValuesRequestF(importMeteringDeviceValuesRequest);
    }
        //Завершающий общий для обеих типов счетчиков код
    private static void ImportMeteringDeviceValuesRequestF(ImportMeteringDeviceValuesRequest importMeteringDeviceValuesRequest) {
        DeviceMeteringService service = new DeviceMeteringService();
        DeviceMeteringPortTypes port = service.getDeviceMeteringPort();
        BindingProvider bp = (BindingProvider) port;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, prop.getProperty("ENDPOINT") + "/ext-bus-device-metering-service/services/DeviceMetering");
        //заголовок
        RequestHeader requestHeader = new RequestHeader();
        //поле с датой
        Date dt = new Date();
        XMLGregorianCalendar date2 = getGrigorian(dt);
        requestHeader.setDate(date2);
        requestHeader.setMessageGUID(java.util.UUID.randomUUID().toString());
        // закончили с датой
        requestHeader.setOrgPPAGUID(prop.getProperty("OrgPPAGUID"));
        requestHeader.setIsOperatorSignature(true);
        Holder holder = new Holder<HeaderType>();
        //делаем запрос и принимаем ответ.
        try {
            ImportResult res = port.importMeteringDeviceValues(importMeteringDeviceValuesRequest, requestHeader, holder);
            List<CommonResultType> commonResultTypeList = res.getCommonResult();
            CommonResultType commonResultType = commonResultTypeList.get(0);
            System.out.println(commonResultType.getUniqueNumber());

        } catch (Exception e) {
            System.err.println("Error occurred importMeteringDeviceValues");
            e.printStackTrace();
        }
    }
    private static ImportMeteringDeviceValuesRequest newDeviceRequestObj (String fIASHouseGuid){
        //создаем объект ImportMeteringDeviceValuesRequest общий для Electric и OneRate devices
        //тело запроса
        ImportMeteringDeviceValuesRequest importMeteringDeviceValuesRequest = new ImportMeteringDeviceValuesRequest();
        importMeteringDeviceValuesRequest.setVersion("10.0.1.1");
        importMeteringDeviceValuesRequest.setId("1");//по ID процедура подписи находит подписываемый элемент
        importMeteringDeviceValuesRequest.setFIASHouseGuid(fIASHouseGuid);
        return importMeteringDeviceValuesRequest;
    }
    //сохранение в базу результата ExportMeteringDeviceDataRequest
    public static void ExportMeteringDeviceDataRequestSave(ExportMeteringDeviceDataResult res) {
        String tableName = "metdev";
        Connection conn = null;
        try {
            conn = database.dbfJDBCConnection();
        } catch (Exception e) {
            System.err.println("Error occurred connection"+tableName+".dbf");
            e.printStackTrace();
        }

        List<ExportMeteringDeviceDataResultType> meterDeviceList = res.getMeteringDevice();
        for (int i = 0; i < meterDeviceList.size(); i++) { //цикл по NsiElement в xml ответе

            ExportMeteringDeviceDataResultType meterDevice = meterDeviceList.get(i);
            //нет ли в базе такого счетчика
            int dbRecordCount = recordExist(conn, tableName, "rootguid", meterDevice.getMeteringDeviceRootGUID(),true);
            MeteringDeviceBasicCharacteristicsType basicCharacter = meterDevice.getBasicChatacteristicts();

            String accountGUID = "";
            String sQLUpdateAddon = "";
            String sQLInsertAddon1 = "";
            String sQLInsertAddon2 = "";
            //Характеристики ИПУ жилого помещения (значение справочника "Вид прибора учета" = индивидуальный)
            if (basicCharacter.getResidentialPremiseDevice() != null) {
                //индивидуальный счетчик
                //Лицевых счетов на счетчике может быть несколько, берем 1
                accountGUID = basicCharacter.getResidentialPremiseDevice().getAccountGUID().get(0);
            }
            //Характеристики общеквартирного ПУ (для квартир коммунального заселения) (значение справочника "Вид прибора учета" = Общий (квартирный))
            if (basicCharacter.getCollectiveApartmentDevice() != null) {
                accountGUID = basicCharacter.getCollectiveApartmentDevice().getAccountGUID().get(0);
            }
            //Характеристики ИПУ жилого дома (значение справочника "Вид прибора учета" = индивидуальный, тип дома = жилой дом)
            if (basicCharacter.getApartmentHouseDevice() != null) {
                accountGUID = basicCharacter.getApartmentHouseDevice().getAccountGUID().get(0);
            }
            sQLUpdateAddon = ", metdv27 = 1, accoutguid = '" + accountGUID + "' ";
            sQLInsertAddon1 = ",metdv27,accoutguid";
            sQLInsertAddon2 = ",1,'" + accountGUID + "' ";

            //достаем информацию о счетчике энергии
            MunicipalResourceElectricType municResurseE = meterDevice.getMunicipalResourceEnergy();
            String sql = null;
            if (municResurseE != null) { //В xml ответе есть инфо о счетчике энергии
                if (dbRecordCount > 0) {//в базе уже есть этот счетчик
                    sql = "UPDATE " + tableName
                            + " SET komres2 = 3"
                            +sQLUpdateAddon +
                            " WHERE rootguid = '" + meterDevice.getMeteringDeviceRootGUID() + "'";

                } else {
                    sql = "INSERT INTO " + tableName
                            + "(rootguid, komres2" + sQLInsertAddon1 + ")"
                            + " VALUES ("
                            + "'" + meterDevice.getMeteringDeviceRootGUID() + "',"
                            + "3" + sQLInsertAddon2 + ")";
                }
                database.updateSQL(conn, sql);
            }

           //достаем информацию о счетчике не энергии
            List<MunicipalResourceNotElectricType> municResurseNE = meterDevice.getMunicipalResourceNotEnergy();
            if (municResurseNE.size() > 0) {
                if (dbRecordCount > 0) {//в базе уже есть этот счетчик
                    sql = "UPDATE " + tableName
                            + " SET "
                            + "komres2 = " + municResurseNE.get(0).getMunicipalResource().getCode()
                            + sQLUpdateAddon + ")"
                            + " WHERE rootguid = '" + meterDevice.getMeteringDeviceRootGUID() + "'";
                } else {
                    sql = "INSERT INTO " + tableName
                            + "(rootguid, komres2" + sQLInsertAddon1 + ")"
                            + " VALUES ('" + meterDevice.getMeteringDeviceRootGUID() + "',"
                            + municResurseNE.get(0).getMunicipalResource().getCode()
                            + sQLInsertAddon2 + ")";
                }
                database.updateSQL(conn, sql);
            }
        }//цикл по списку счетчиков

        try {
            conn.close();
        } catch (Exception e) {
            System.err.println("Error occurred close connection metdevt.dbf");
            e.printStackTrace();
        }
    }
    //получение параметров счетчиков
    public static void ExportMeteringDeviceDataRequest(String fIASHouseGuid,String meteringDeviceTypeCode,String meteringDeviceTypeGUID,String meteringDeviceRootGUID) {

        logs();//подключаем логгирование
        HouseManagementService service = new HouseManagementService();
        HouseManagementPortsType port = service.getHouseManagementPort();
        BindingProvider bp = (BindingProvider) port;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, prop.getProperty("ENDPOINT")+"/ext-bus-home-management-service/services/HomeManagement");
        //готовим данные для запроса
        //тело
        ExportMeteringDeviceDataRequest dr = new ExportMeteringDeviceDataRequest();
        dr.setFIASHouseGuid(fIASHouseGuid);
        dr.setVersion("10.0.1.1");
        dr.setId("1");
        dr.setMeteringDeviceRootGUID(meteringDeviceRootGUID);
        //все счетчики определенного типа
        if (meteringDeviceTypeCode!=null&&meteringDeviceTypeGUID!=null) {
            NsiRef nsr = new NsiRef();
            nsr.setCode(meteringDeviceTypeCode); //справочник 27
            nsr.setGUID(meteringDeviceTypeGUID);
            dr.setMeteringDeviceType(nsr);
        }
        //заголовок
        RequestHeader requestHeader = new RequestHeader();
        //поле с датой
        requestHeader.setDate(getGrigorian(new Date()));
        requestHeader.setMessageGUID(java.util.UUID.randomUUID().toString());
        // end date set
        requestHeader.setOrgPPAGUID(prop.getProperty("OrgPPAGUID"));
        requestHeader.setIsOperatorSignature(true);
        //end header
        Holder holder = new Holder<HeaderType>();

        //делаем запрос и принимаем ответ.

        try {
            ExportMeteringDeviceDataResult res = port.exportMeteringDeviceData(dr, requestHeader, holder);
            ExportMeteringDeviceDataRequestSave(res);

        } catch (Exception e) {
            System.err.println("Error occurred while exportMeteringDeviceData");
            e.printStackTrace();
        }

    }

}
