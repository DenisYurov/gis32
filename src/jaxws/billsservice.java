package jaxws;

import database.database;
import dataobject.orgDAO;
import javafx.beans.property.SimpleDoubleProperty;
import ru.gosuslugi.dom.schema.integration.base.CommonResultType;
import ru.gosuslugi.dom.schema.integration.base.HeaderType;
import ru.gosuslugi.dom.schema.integration.base.ImportResult;
import ru.gosuslugi.dom.schema.integration.base.RequestHeader;
import ru.gosuslugi.dom.schema.integration.bills.*;
import ru.gosuslugi.dom.schema.integration.bills_service.BillsPortsType;
import ru.gosuslugi.dom.schema.integration.bills_service.BillsService;
import ru.gosuslugi.dom.schema.integration.device_metering_service.DeviceMeteringPortTypes;
import ru.gosuslugi.dom.schema.integration.device_metering_service.DeviceMeteringService;
import ru.gosuslugi.dom.schema.integration.house_management.ExportAccountResult;
import ru.gosuslugi.dom.schema.integration.house_management.ExportAccountResultType;
import ru.gosuslugi.dom.schema.integration.nsi_base.NsiRef;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static database.database.recordExist;
import static dataobject.billsoaplogDAO.deleteAllBillSoapLog;
import static dataobject.billsoaplogDAO.insertbillsoaplogDAOErr;
import static dataobject.municservDAO.getMunicServ;
import static jaxws.jaxutil.getGrigorian;
import static jaxws.jaxutil.logs;

/**
 * Created by dionis on 30.10.2016.
 */
public class billsservice {
    public static Properties prop;
    private static double billProgressDelta;
    //положение прогрессбара
    public static volatile SimpleDoubleProperty progress;

    public static void allBillSend() {
        //logs();//подключаем логгирование
        //вычисляем дельту прогрессбара
        billProgressDelta = (0.8 / dataobject.billdocDAO.getBillRecCount());
        //Получаем из таблицы billdoc список организаций по датам по которым есть платежи
        ResultSet org = dataobject.billdocDAO.getOrgDataNotSentBill();
        try {
            deleteAllBillSoapLog();//очистка журнала целых сообщений
            while (org.next()) {
                importPaymentDocumentData(org.getString("orgppaguid"), org.getInt("month"), org.getInt("year"));
            }
            // после отправки всех платежей и приема ответов
            progress.set(progress.get() + 0.2);
        } catch (Exception e) {
            System.err.println("Error occurred in allBillLoop");
            e.printStackTrace();
        }
    }

    //Отправка в гис начислений
    public static void importPaymentDocumentData(String orgPPAGUID, int month, int year) {

        try {
            BillsService service = new BillsService();
            BillsPortsType port = service.getBillsPort();
            BindingProvider bp = (BindingProvider) port;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, prop.getProperty("ENDPOINT") + "/ext-bus-bills-service/services/Bills");
            //заголовок
            RequestHeader requestHeader = new RequestHeader();
            //поле с датой
            //XMLGregorianCalendar date2 = getGrigorian(new Date());
            requestHeader.setDate(getGrigorian(new Date()));
            requestHeader.setMessageGUID(java.util.UUID.randomUUID().toString());
            // закончили с датой
            requestHeader.setOrgPPAGUID(orgPPAGUID);
            requestHeader.setIsOperatorSignature(true);
            Holder holder = new Holder<HeaderType>();

            String paymentInfoGUID = java.util.UUID.randomUUID().toString();
            ImportPaymentDocumentRequest importPaymentDocumentRequest = buildPaymentInformation(month, year, paymentInfoGUID);
            //готовим список для записи транспортных GUID PaymentDocument - ов
            List<String> transportGUIDList = new ArrayList<>();
            List<String> accountGUIDList = new ArrayList<>();
            //Размещаемый платежный документ. 1-1000
            //paymentDocument
            //извлекаем список платежей из billdoc одной организации за определенный месяц и год.
            ResultSet billRs = dataobject.billdocDAO.getBillbyOrgNData(orgPPAGUID, month, year);
            int i = 0;//счетчик 1000 -го документа
            //цикл по платежам
            while (billRs.next()) {
                ImportPaymentDocumentRequest.PaymentDocument paymentDocument = paymentDoc(billRs, transportGUIDList, accountGUIDList, paymentInfoGUID);
                importPaymentDocumentRequest.getPaymentDocument().add(paymentDocument);
                i++;
                //добавлен  1000-ный paymentDocument. В одном запросе больше нельзя. Отправляем
                if (i == 1) {
                    //делаем запрос и принимаем ответ.
                    sendSoap(port, importPaymentDocumentRequest, requestHeader, holder, transportGUIDList, accountGUIDList, orgPPAGUID);
                    //готовим новое сообщение
                    //новый message GUID в заголовке
                    requestHeader.setMessageGUID(java.util.UUID.randomUUID().toString());
                    //новое сообщение
                    paymentInfoGUID = java.util.UUID.randomUUID().toString();
                    importPaymentDocumentRequest = buildPaymentInformation(month, year, paymentInfoGUID);
                    importPaymentDocumentRequest.getPaymentDocument().clear();
                    i = 0;
                }
                progress.set(progress.get() + billProgressDelta);
            }
            //либо ни одного прохода while не было, либо выход из while произешел на 1000 paymentDocument. Его мы уже отправили.
            if (i != 0) {
                //делаем запрос и принимаем ответ.
                sendSoap(port, importPaymentDocumentRequest, requestHeader, holder, transportGUIDList, accountGUIDList, orgPPAGUID);
            }

        } catch (Exception e) {
            System.err.println("Error occurred importPaymentDocumentData");
            e.printStackTrace();
            /*
            if (e.getMessage().toLowerCase().contains("AUT011009".toLowerCase())) {
                dataobject.orgDAO.setDataProvider(orgPPAGUID, false);
            }
            */
        }
    }
//формируем importPaymentDocumentRequest часть soap запроса
    private static ImportPaymentDocumentRequest buildPaymentInformation(int month, int year, String paymentInfoGUID) {
        ImportPaymentDocumentRequest importPaymentDocumentRequest = new ImportPaymentDocumentRequest();
        importPaymentDocumentRequest.setVersion("10.0.1.1");
        importPaymentDocumentRequest.setId("1");
        importPaymentDocumentRequest.setMonth(month);
        importPaymentDocumentRequest.setYear((short) year);
        //Сведения о платежных реквизитах получателя - бизнес-ключ поиска размещенных платежных реквизитов в ГИС ЖКХ
        ImportPaymentDocumentRequest.PaymentInformation paymentInformation = new ImportPaymentDocumentRequest.PaymentInformation();
        //String paymentInfoGUID = java.util.UUID.randomUUID().toString();
        paymentInformation.setTransportGUID(paymentInfoGUID);
        paymentInformation.setBankBIK(prop.getProperty("BIK"));
        paymentInformation.setOperatingAccountNumber(prop.getProperty("operatingAccount"));
        importPaymentDocumentRequest.getPaymentInformation().add(paymentInformation);//1 and more
        return importPaymentDocumentRequest;
    }

    //делаем soap запрос в ГИС и принимаем ответ.
    private static void sendSoap(BillsPortsType port, ImportPaymentDocumentRequest importPaymentDocumentRequest, RequestHeader requestHeader, Holder holder, List<String> transportGUIDList, List<String> accountGUIDList, String orgPPAGUID) throws Exception {
        try {
            ImportResult res = port.importPaymentDocumentData(importPaymentDocumentRequest, requestHeader, holder);
            insertTransportGUID("bill.billdoc", transportGUIDList, accountGUIDList);//запись в базу transportguid ов запроса
            importPaymentDocumentDataSave(res, orgPPAGUID);//запись результата запроса в базу


        } catch (Exception e) {
            System.err.println("Error occurred importPaymentDocumentData send soap");
            e.printStackTrace();
            //добавление ошибок целого сообщения в журнал
            insertbillsoaplogDAOErr(requestHeader.getOrgPPAGUID(), requestHeader.getMessageGUID(),e.getMessage());
        }
    }

    //собираем PaymentDocument из ResultSet billRs
    private static ImportPaymentDocumentRequest.PaymentDocument paymentDoc(ResultSet billRs, List<String> transportGUIDList, List<String> accountGUIDList, String paymentInfoGUID) throws Exception {
        ImportPaymentDocumentRequest.PaymentDocument paymentDocument = new ImportPaymentDocumentRequest.PaymentDocument();
        //генерируем очередной трансп GUID
        String pDocTranGUID = java.util.UUID.randomUUID().toString();
        transportGUIDList.add(pDocTranGUID);
        paymentDocument.setTransportGUID(pDocTranGUID);
        //Идентификатор лицевого счета
        String accountGUID = billRs.getString("accoguid");
        accountGUIDList.add(accountGUID);
        paymentDocument.setAccountGuid(accountGUID);
        //не обязательно
        //Номер платежного документа, по которому внесена плата, присвоенный такому документу исполнителем в целях осуществления расчетов по внесению платы
        paymentDocument.setPaymentDocumentNumber(billRs.getString("ourdocnum"));

        //Адресные сведения
        PaymentDocumentType.AddressInfo addressInfo = new PaymentDocumentType.AddressInfo();
        //Количество проживающих. Не обязательно
        if (billRs.getInt("livingpers") != 0) {
            addressInfo.setLivingPersonsNumber(billRs.getByte("livingpers"));
        }
        //Жилая площадь. Не обязательно
        //addressInfo.setResidentialSquare();
        //Отапливаемая площадь. Не обязательно
        //addressInfo.setHeatedArea();
        //Общая площадь для ЛС. обязательно
        //BigDecimal totalSquare = new BigDecimal("110.2");
        addressInfo.setTotalSquare(billRs.getBigDecimal("totalsquar"));
        //end AddresInfo
        paymentDocument.setAddressInfo(addressInfo);//добавился блок addresInfo
        //Начисление по жилищной услуге. Выбираем из таблицы jilserv все записи по accountGUID
        readChargeService(accountGUID, paymentDocument);
        //Начисление по капитальному ремонту. Выбираем из таблицы kapserv все записи по accountGUID
        //readKapService(accountGUID, paymentDocument);
        //муниципальная услуга. Выбираем из таблицы все записи по accountGUID
        readMunicipalService(accountGUID, paymentDocument);
        //дополнительная услуга
        readAdditionalService(accountGUID, paymentDocument);

        //paymentDocument.setExpose(true);//выставляем если не выставить - статус документа будет проект, который легко удаляется
        paymentDocument.setDebtPreviousPeriods(billRs.getBigDecimal("debt"));//долг
        paymentDocument.setPaymentInformationKey(paymentInfoGUID);
        return paymentDocument;
    }
    private static void readKapService(String accountGUID, ImportPaymentDocumentRequest.PaymentDocument paymentDocument) throws Exception {
        PaymentDocumentType.ChargeInfo chargeInfo = new PaymentDocumentType.ChargeInfo();
        CapitalRepairType capitalRepairType = new CapitalRepairType();
        capitalRepairType.setContribution(new BigDecimal("10.00"));
        capitalRepairType.setAccountingPeriodTotal(new BigDecimal("11.00"));
        capitalRepairType.setMoneyRecalculation(new BigDecimal("-1.00"));
        capitalRepairType.setMoneyDiscount(new BigDecimal("0.00"));
        capitalRepairType.setTotalPayable(new BigDecimal("12.00"));
        paymentDocument.setCapitalRepairCharge(capitalRepairType);

    }

    //Начисление по жилищным услугам
    private static void readChargeService(String accountGUID, ImportPaymentDocumentRequest.PaymentDocument paymentDocument) throws Exception {
        ResultSet homeRS = dataobject.jilservDAO.getJilServ(accountGUID);
        // Ноль ChargeInfo и более
        while (homeRS.next()) {
            PaymentDocumentType.ChargeInfo chargeInfo = new PaymentDocumentType.ChargeInfo();
            //HousingService или MunicipalService или AdditionalService
            //Жилищная услуга
            PDServiceChargeType.HousingService housingService = new PDServiceChargeType.HousingService();
            NsiRef nsr = new NsiRef();
            nsr.setCode(homeRS.getString("nsicode")); //справочник 50 Жилищная услуга
            //nsr.setGUID("510ed4d6-cf55-4fc1-939a-1c642afe982c"); //УЮТ
            nsr.setGUID(homeRS.getString("nsiguid"));//"cd4ad45b-d226-44fe-ae61-57977f251b57");
            housingService.setServiceType(nsr);
            //BigDecimal rate = new BigDecimal("20.00");
            housingService.setRate(homeRS.getBigDecimal("rate"));
            housingService.setTotalPayable(homeRS.getBigDecimal("totalpay"));
            housingService.setAccountingPeriodTotal(homeRS.getBigDecimal("acctotal"));
            //String src = homeRS.getString("explan");
            //String name1 = new String(src.getBytes("Cp1251"), "UTF-8");
            //обязятельное поле
            if (homeRS.getString("explan") != null && !homeRS.getString("explan").equals("")) {
                housingService.setCalcExplanation(homeRS.getString("explan"));
            } else {
                housingService.setCalcExplanation("_");
            }
            PDServiceChargeType.HousingService.Consumption housServConsump = new PDServiceChargeType.HousingService.Consumption();
            PDServiceChargeType.HousingService.Consumption.Volume hServConsVol = new PDServiceChargeType.HousingService.Consumption.Volume();

            //индивидуальный consumption, индивидульное потребление
            if (homeRS.getString("cnsindval") != null && homeRS.getBigDecimal("cnsindval").compareTo(BigDecimal.ZERO) != 0) {
                hServConsVol.setType("I");
                hServConsVol.setValue(homeRS.getBigDecimal("cnsindval"));// Объем потребленной услуги
                housServConsump.setVolume(hServConsVol);
                housingService.setConsumption(housServConsump);
            }
            //общедомовой consumption, общедомовые нужды
            if (homeRS.getString("cnsobshval") != null && homeRS.getBigDecimal("cnsobshval").compareTo(BigDecimal.ZERO) != 0) {
                hServConsVol.setType("O");
                hServConsVol.setValue(homeRS.getBigDecimal("cnsobshval"));// Объем потребленной услуги
                housServConsump.setVolume(hServConsVol);
                housingService.setConsumption(housServConsump);

            }

            chargeInfo.setHousingService(housingService);
            paymentDocument.getChargeInfo().add(chargeInfo);//добавляем блок chargeInfo к PaymentDocument
        }
    }

    //добавление муниципальных услуг из таблицы municserv
    private static void readMunicipalService(String accountGUID, ImportPaymentDocumentRequest.PaymentDocument paymentDocument) throws Exception {
        ResultSet rs = getMunicServ(accountGUID);
        boolean fl = false;
        while (rs.next()) {
            //Начисление по услуге. Ноль ChargeInfo и более
            PaymentDocumentType.ChargeInfo chargeInfo = new PaymentDocumentType.ChargeInfo();
            //Муниципальная услуга
            PDServiceChargeType.MunicipalService municipalService = new PDServiceChargeType.MunicipalService();
            NsiRef nsr = new NsiRef();
            nsr.setCode(rs.getString("nsicode")); //справочник 3  услуга
            nsr.setGUID(rs.getString("nsiguid"));
            municipalService.setServiceType(nsr);
            municipalService.setRate(rs.getBigDecimal("rate"));
            municipalService.setTotalPayable(rs.getBigDecimal("totalpay"));
            municipalService.setAccountingPeriodTotal(rs.getBigDecimal("acctotal"));
            //recalculation
            if (rs.getInt("recalc") != 0) {
                ServiceChargeType serviceChargeType = new ServiceChargeType();
                serviceChargeType.setMoneyRecalculation(rs.getBigDecimal("recalc"));
                municipalService.setServiceCharge(serviceChargeType);

                if (rs.getString("recalcreas") != null && !rs.getString("recalcreas").isEmpty()) {
                    PDServiceChargeType.MunicipalService.PaymentRecalculation paymentRecalculation = new PDServiceChargeType.MunicipalService.PaymentRecalculation();
                    paymentRecalculation.setRecalculationReason(rs.getString("recalcreas"));
                    paymentRecalculation.setSum(rs.getBigDecimal("recalcsum"));

                    municipalService.setPaymentRecalculation(paymentRecalculation);
                }
            }
            //consumption
            PDServiceChargeType.MunicipalService.Consumption mServConsumption = new PDServiceChargeType.MunicipalService.Consumption();
            PDServiceChargeType.MunicipalService.Consumption.Volume mServConsVol = new PDServiceChargeType.MunicipalService.Consumption.Volume();
            //индивидуальный consumption, индивидульное потребление

            if (rs.getString("cnsindmetd") != null && !rs.getString("cnsindmetd").isEmpty()) {
                mServConsVol.setType("I");
                mServConsVol.setValue(rs.getBigDecimal("cnsindval"));// Объем потребленной услуги
                mServConsVol.setDeterminingMethod(rs.getString("cnsindmetd").toUpperCase());
                mServConsumption.getVolume().add(mServConsVol);
                fl = true;
            }
            //общедомовой consumption, общедомовые нужды
            if (rs.getString("cnsobhmetd") != null && !rs.getString("cnsobhmetd").isEmpty()) {
                PDServiceChargeType.MunicipalService.Consumption.Volume mServConsVol1 = new PDServiceChargeType.MunicipalService.Consumption.Volume();
                mServConsVol1.setType("O");
                mServConsVol1.setValue(rs.getBigDecimal("cnsobshval"));// Объем потребленной услуги
                mServConsVol1.setDeterminingMethod(rs.getString("cnsobhmetd").toUpperCase());
                mServConsumption.getVolume().add(mServConsVol1);
                fl = true;
            }
            if (fl) {
                municipalService.setConsumption(mServConsumption);
            }
            String explain = rs.getString("explan");
            if (explain == null) {
                explain = "_";
            }
            municipalService.setCalcExplanation(explain);
            //service Information
            ServiceInformation serviceInformation = new ServiceInformation();
            fl = false;
            if (rs.getString("sinfindval") != null && rs.getBigDecimal("sinfindval").compareTo(BigDecimal.ZERO) != 0) {
                //ServiceInformation serviceInformation = new ServiceInformation();
                serviceInformation.setIndividualConsumptionCurrentValue(rs.getBigDecimal("sinfindval"));
                fl = true;
            }
            if (rs.getString("sinfhonval") != null && rs.getBigDecimal("sinfhonval").compareTo(BigDecimal.ZERO) != 0) {
                serviceInformation.setHouseOverallNeedsCurrentValue(rs.getBigDecimal("sinfhonval"));
                fl = true;
            }
            if (rs.getString("sinfhotin") != null && rs.getBigDecimal("sinfhotin").compareTo(BigDecimal.ZERO) != 0) {
                serviceInformation.setHouseTotalIndividualConsumption(rs.getBigDecimal("sinfhotin"));
                fl = true;
            }
            if (rs.getString("sinfhotnee") != null && rs.getBigDecimal("sinfhotnee").compareTo(BigDecimal.ZERO) != 0) {
                serviceInformation.setHouseTotalHouseOverallNeeds(rs.getBigDecimal("sinfhotnee"));
                fl = true;
            }
            if (rs.getString("sinfhonnr") != null && rs.getBigDecimal("sinfhonnr").compareTo(BigDecimal.ZERO) != 0) {
                serviceInformation.setHouseOverallNeedsNorm(rs.getBigDecimal("sinfhonnr"));
                fl = true;
            }
            if (rs.getString("sinfincnor") != null && rs.getBigDecimal("sinfincnor").compareTo(BigDecimal.ZERO) != 0) {
                serviceInformation.setIndividualConsumptionNorm(rs.getBigDecimal("sinfincnor"));
                fl = true;
            }
            if (fl) {
                municipalService.setServiceInformation(serviceInformation);
            }
            chargeInfo.setMunicipalService(municipalService);

            paymentDocument.getChargeInfo().add(chargeInfo);//добавляем очередной блок chargeInfo к PaymentDocument
        }
    }

    //запись списка транспортных GUID элементов запроса
    public static void insertTransportGUID(String tableName, List<String> transportGUIDList, List<String> accountGUIDList) {
        Connection conn = null;
        try {
            conn = database.dbfJDBCConnection();
            String sql;
            for (int i = 0; i < transportGUIDList.size(); i++) {
                sql = "UPDATE " + tableName
                        + " SET transguid = " + "'" + transportGUIDList.get(i) + "'"
                        + " WHERE accoguid = '" + accountGUIDList.get(i) + "'";
                database.updateSQL(conn, sql);
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Error occurred in insertTransportGUID");
            e.printStackTrace();
        }

    }

    // запись в таблицу billdoc результатов экспорта.
    public static void importPaymentDocumentDataSave(ImportResult res, String orgPPAGUID) {
        String tableName = "bill.billdoc";
        Connection conn = null;
        try {
            conn = database.dbfJDBCConnection();
        } catch (Exception e) {
            System.err.println("Error occurred connection" + tableName + ".dbf");
            e.printStackTrace();
        }
        List<CommonResultType> importResultList = res.getCommonResult();
        for (int i = 0; i < importResultList.size(); i++) { //цикл по Accounts в xml ответе
            CommonResultType importResult = importResultList.get(i);
            String sql;
            //разбор ошибок
            String errString = "";
            List<CommonResultType.Error> errList = importResult.getError();
            //собираем все ошибки в одну строку
            for (i = 0; i < errList.size(); i++) {
                errString = errString + errList.get(i).getDescription() + " ";
            }

            sql = "UPDATE " + tableName
                    + " SET billid = '" + importResult.getUniqueNumber() + "',"
                    + " error = '" + errString + "'"
                    + " WHERE transguid = '" + importResult.getTransportGUID() + "' AND orgppaguid = '" + orgPPAGUID + "'";
            database.updateSQL(conn, sql);
        }
        try {
            conn.close();
        } catch (Exception e) {
            System.err.println("Error occurred close " + tableName + ".dbf");
            e.printStackTrace();
        }
    }

    //чтение additional service.
    private static void readAdditionalService(String accountGUID, ImportPaymentDocumentRequest.PaymentDocument paymentDocument) throws Exception {
        Connection conn = database.dbfJDBCConnection();
        PreparedStatement pstat = conn.prepareStatement("SELECT * FROM bill.addicserv WHERE accoguid=?");
        pstat.setString(1, accountGUID);
        ResultSet rs = pstat.executeQuery();

        while (rs.next()) {
            //Начисление по услуге. Ноль ChargeInfo и более
            PaymentDocumentType.ChargeInfo chargeInfo = new PaymentDocumentType.ChargeInfo();
            PDServiceChargeType.AdditionalService additionalService = new PDServiceChargeType.AdditionalService();
            NsiRef nsr = new NsiRef();
            nsr.setCode(rs.getString("nsicode")); //справочник 1 дополнительная услуга
            nsr.setGUID(rs.getString("nsiguid"));
            additionalService.setServiceType(nsr);
            additionalService.setRate(rs.getBigDecimal("rate"));
            additionalService.setTotalPayable(rs.getBigDecimal("totalpay"));
            additionalService.setAccountingPeriodTotal(rs.getBigDecimal("acctotal"));
            String explain = rs.getString("explan");
            if (explain == null) {
                explain = " ";
            }
            additionalService.setCalcExplanation(explain);
            if (rs.getString("addorguid") != null) {
                additionalService.setOrgPPAGUID(rs.getString("addorguid"));
            }
            //новый ServiceChargeType
            ServiceChargeType serviceChargeType = new ServiceChargeType();
            // добавляем recalculation если есть
            if (rs.getInt("recalc") != 0) {
                serviceChargeType.setMoneyRecalculation(rs.getBigDecimal("recalc"));
                additionalService.setServiceCharge(serviceChargeType);
            }
            // добавляем discount если есть
            if (rs.getInt("discount") != 0) {
                serviceChargeType.setMoneyDiscount(rs.getBigDecimal("discount"));
                additionalService.setServiceCharge(serviceChargeType);
            }
            //consumption
            PDServiceChargeType.AdditionalService.Consumption mServConsumption = new PDServiceChargeType.AdditionalService.Consumption();
            PDServiceChargeType.AdditionalService.Consumption.Volume mServConsVol = new PDServiceChargeType.AdditionalService.Consumption.Volume();
            //индивидуальный consumption, индивидульное потребление
            if (rs.getString("cnsindval") != null && !rs.getString("cnsindval").isEmpty()) {
                mServConsVol.setType("I");
                mServConsVol.setValue(rs.getBigDecimal("cnsindval"));// Объем потребленной услуги
                mServConsumption.getVolume().add(mServConsVol);
            }
            //общедомовой consumption
            if (rs.getString("cnsobshval") != null && !rs.getString("cnsobshval").isEmpty()) {
                mServConsVol.setType("O");
                mServConsVol.setValue(rs.getBigDecimal("cnsobshval"));// Объем потребленной услуги
                mServConsumption.getVolume().add(mServConsVol);
            }

            chargeInfo.setAdditionalService(additionalService);
            paymentDocument.getChargeInfo().add(chargeInfo);//добавляем очередной блок chargeInfo к PaymentDocument
        }
        conn.close();
    }

    //чтение долгов из таблицы chargedebt. Долги расписанные по услугам. Недоделано
    private static void readChargeDebt(String accountGUID, ImportPaymentDocumentRequest.PaymentDocument paymentDocument) throws Exception {
        Connection conn = database.dbfJDBCConnection();
        PreparedStatement pstat = conn.prepareStatement("SELECT * FROM bill.addicserv WHERE accoguid=?");
        pstat.setString(1, accountGUID);
        ResultSet rs = pstat.executeQuery();
        while (rs.next()) {

            if (rs.getString("chdebtype") != null && rs.getString("chdebtype") == "M") {
            }
            ;
            //долг HousingService
            if (rs.getString("chdebtype") != null && rs.getString("chdebtype") == "H") {
            }
            //долг за Additional Service
            if (rs.getString("chdebtype") != null && rs.getString("chdebtype") == "A") {
            }
            ;
            //paymentDocument.setDebtPreviousPeriods();
        }
    }
}
