package jaxws;

import database.database;
import javafx.beans.property.SimpleDoubleProperty;
import ru.gosuslugi.dom.schema.integration.base.HeaderType;
import ru.gosuslugi.dom.schema.integration.organizations_registry_common.*;
import ru.gosuslugi.dom.schema.integration.organizations_registry_common_service.RegOrgPortsType;
import ru.gosuslugi.dom.schema.integration.organizations_registry_common_service.RegOrgService;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static jaxws.jaxutil.getGrigorian;
import static jaxws.jaxutil.logs;

/**
 * Created by dionis on 03.11.2016.
 */
public class regorg {
    public static Properties prop;
    public static SimpleDoubleProperty progress;
    //получение организации по ОГРН
    public static void exportOrgRegistryRequest(String ogrn) {
        //logs();//подключаем логгирование
        if (ogrn == null&& ogrn.isEmpty()){ return; }
        RegOrgService service = new RegOrgService();
        RegOrgPortsType port = service.getRegOrgPort();
        BindingProvider bp = (BindingProvider) port;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, prop.getProperty("ENDPOINT") + "/ext-bus-org-registry-common-service/services/OrgRegistryCommon");
        //заголовок
        HeaderType requestHeader = new HeaderType();
        //поле с датой
        Date dt = new Date();
        XMLGregorianCalendar date2 = getGrigorian(dt);
        requestHeader.setDate(date2);
        requestHeader.setMessageGUID(java.util.UUID.randomUUID().toString());
        // закончили с датой
        Holder holder = new Holder<HeaderType>();
        ExportOrgRegistryRequest exportOrgRegistryRequest = new ExportOrgRegistryRequest();
        exportOrgRegistryRequest.setVersion("10.0.2.1");
        exportOrgRegistryRequest.setId("1");
        ExportOrgRegistryRequest.SearchCriteria searchCriteria = new ExportOrgRegistryRequest.SearchCriteria();
        searchCriteria.setOGRN(ogrn);
        exportOrgRegistryRequest.getSearchCriteria().add(searchCriteria);
        try {
            ExportOrgRegistryResult res = port.exportOrgRegistry(exportOrgRegistryRequest, requestHeader, holder);
            updateOrg(res);
        } catch (Exception e) {
            System.err.println("Error occurred importPaymentDocumentData");
            e.printStackTrace();
        }
    }
    //
    public static void updateOrg(ExportOrgRegistryResult res) {
        Connection conn = null;
        List<ExportOrgRegistryResultType> orgRegList = res.getOrgData();
        try {
            conn = database.dbfJDBCConnection();
            String sql;
            for (int i = 0; i < orgRegList.size(); i++) {
                ExportOrgRegistryResultType exportOrgRegistryResultType = orgRegList.get(i);
                String name = exportOrgRegistryResultType.getOrgVersion().getLegal().getShortName();
                if (name == null || name.isEmpty()){
                    exportOrgRegistryResultType.getOrgVersion().getLegal().getFullName();
                }
                sql = "UPDATE org"
                        + " SET orgppaguid = " + "'" + exportOrgRegistryResultType.getOrgPPAGUID() + "',"
                        + " name = " + "'" + name + "',"
                        + " inn = " + "'" + exportOrgRegistryResultType.getOrgVersion().getLegal().getINN() + "'"
                        + " WHERE trim(ogrn) = '" + exportOrgRegistryResultType.getOrgVersion().getLegal().getOGRN().trim() + "'";

                database.updateSQL(conn, sql);
                // sql = "UPDATE org"
               //         + " SET orgppaguid = ?"
                        //+ " name = ?"
                        //+ " inn = ?,"
                       // + " kpp = ?"
                //        + " WHERE ogrn = ?";
                //PreparedStatement pstat = conn.prepareStatement(sql);
                //pstat.setString(1, exportOrgRegistryResultType.getOrgPPAGUID());
                //pstat.setString(2, exportOrgRegistryResultType.getOrgVersion().getLegal().getFullName());
                //pstat.setString(3, exportOrgRegistryResultType.getOrgVersion().getLegal().getINN());
                //pstat.setString(4, exportOrgRegistryResultType.getOrgVersion().getLegal().getKPP());
                //pstat.setString(2,exportOrgRegistryResultType.getOrgVersion().getLegal().getOGRN());
                //pstat.executeUpdate();

            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Error occurred in insertTransportGUID");
            e.printStackTrace();
        }

    }
    //техподдержка ГИС говорит что это не сработает
    public static void exportDataProvider() {
        logs();//подключаем логгирование
        RegOrgService service = new RegOrgService();
        RegOrgPortsType port = service.getRegOrgPort();
        BindingProvider bp = (BindingProvider) port;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, prop.getProperty("ENDPOINT") + "/ext-bus-org-registry-common-service/services/OrgRegistryCommon");
        //заголовок
        HeaderType requestHeader = new HeaderType();

        //поле с датой
        Date dt = new Date();
        XMLGregorianCalendar date2 = getGrigorian(dt);
        requestHeader.setDate(date2);
        requestHeader.setMessageGUID(java.util.UUID.randomUUID().toString());
        // закончили с датой
        Holder holder = new Holder<HeaderType>();
        ExportDataProviderRequest exportDataProviderRequest = new ExportDataProviderRequest();
        exportDataProviderRequest.setVersion("10.0.2.1");
        exportDataProviderRequest.setId("1");

        try {
            ExportDataProviderResult res = port.exportDataProvider(exportDataProviderRequest, requestHeader, holder);
        } catch (Exception e) {
            System.err.println("Error occurred importPaymentDocumentData");
            e.printStackTrace();
        }
        ;
    }
}
