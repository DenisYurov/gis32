package jaxws;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMResult;
import java.io.IOException;
import java.util.*;

import org.w3c.dom.Document;


/**
 * Created by dionis on 15.10.2016.
 */
public class jaxutil {

    public static void logs() {
       // System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
       // System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
       // System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dumpTreshold", "9999999999");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold","1000000");
    }

    public static XMLGregorianCalendar getGrigorian(Date date) {
        //преобразует тип Data в XMLGregorianCalendar
        GregorianCalendar c = new GregorianCalendar();
        XMLGregorianCalendar date2 = null;
        c.setTime(date);
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        } catch (Exception e) {
            System.err.println("Error occurred GregorianCalendar ");
            e.printStackTrace();
            return null;
        }
    }

    // преобразование soapMessage в DOM
    public static Document toDocument(SOAPMessage soapMsg)
            throws TransformerConfigurationException, TransformerException, SOAPException, IOException {
        Source src = soapMsg.getSOAPPart().getContent();
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        DOMResult result = new DOMResult();
        transformer.transform(src, result);
        return (Document) result.getNode();
    }
}


/* коррекция пространства имен в запросе package-info.java
@javax.xml.bind.annotation.XmlSchema(namespace = "http://dom.gosuslugi.ru/schema/integration/nsi-base/",
        elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED,
        xmlns = {
                @javax.xml.bind.annotation.XmlNs(
                        prefix = "base",
                        namespaceURI = "http://dom.gosuslugi.ru/schema/integration/base/")
        })
package ru.gosuslugi.dom.schema.integration.nsi_base;

 */