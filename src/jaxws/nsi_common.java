package jaxws;

import database.database;
import ru.gosuslugi.dom.schema.integration.base.HeaderType;

import ru.gosuslugi.dom.schema.integration.nsi_base.*;
import ru.gosuslugi.dom.schema.integration.nsi_common.ExportNsiItemRequest;
import ru.gosuslugi.dom.schema.integration.nsi_common.ExportNsiItemResult;
import ru.gosuslugi.dom.schema.integration.nsi_common_service.NsiPortsType;
import ru.gosuslugi.dom.schema.integration.nsi_common_service.NsiService;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import java.math.BigInteger;
import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static database.database.recordExist;
import static dataobject.nsi50DAO.deleteAllJilus50;
import static dataobject.nsi50DAO.insertNsi50;
import static jaxws.jaxutil.getGrigorian;
import static jaxws.jaxutil.logs;

/**
 * Created by dionis on 30.10.2016.
 */
public class nsi_common {
    public static Properties prop;
    //Запрос содержимого справочника из гис. Запись содержимого в таблицу, если есть


    public static void ExportNsiItemRequestF(String registryNumber, String listGroup) {
        logs();
        NsiService nService = new NsiService();
        NsiPortsType port = nService.getNsiPort();
        BindingProvider bp = (BindingProvider) port;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, prop.getProperty("ENDPOINT") + "/ext-bus-nsi-common-service/services/NsiCommon");
        //готовим данные для запроса
        //тело
        ExportNsiItemRequest exportNsiItemRequest = new ExportNsiItemRequest();
        exportNsiItemRequest.setVersion("10.0.1.2");
        exportNsiItemRequest.setId("1");
        BigInteger nsiB = new BigInteger(registryNumber);
        exportNsiItemRequest.setRegistryNumber(nsiB);
        exportNsiItemRequest.setListGroup(listGroup);
        //заголовок
        HeaderType header = new HeaderType();
        //поле с датой
        Date dt = new Date();
        XMLGregorianCalendar date2 = getGrigorian(dt);
        header.setDate(date2);
        header.setMessageGUID(java.util.UUID.randomUUID().toString());
        // end date set
        Holder holder = new Holder<HeaderType>();
        //делаем запрос и принимаем ответ.
        try {
            ExportNsiItemResult res = port.exportNsiItem(exportNsiItemRequest, header, holder);
            NsiItemType nsiItem = res.getNsiItem();
            List<NsiElementType> elementType = nsiItem.getNsiElement();
            //сохраняем
            if (registryNumber == "2" || registryNumber == "27" || registryNumber == "50") {
                ExportNsiItemRequestS27(elementType, registryNumber);
            }
            if (registryNumber == "50") {
                ExportNsiItemRequestS50(elementType);
            }

        } catch (Exception e) {
            System.err.println("Error occurred expportNsiListRequest");
            e.printStackTrace();
        }
    }

    //сохранение справочника 50
    public static void ExportNsiItemRequestS50(List<NsiElementType> elementType) {
        try {
            deleteAllJilus50();
            for (int i = 0; i < elementType.size(); i++) { //цикл по NsiElement в xml ответе
                NsiElementType nsiElementType = elementType.get(i);
                if (nsiElementType.isIsActual()) {
                    //достаем список елементов <NsiElementField>
                    List<NsiElementFieldType> nsiElementFieldTypeList = nsiElementType.getNsiElementField();
                    for (int j = 0; j < nsiElementFieldTypeList.size(); j++) {
                        String src = "Вид жилищной  услуги";
                        String name1 = new String(src.getBytes("Cp1251"), "UTF-8");//без этого бреда не работает
                        if (nsiElementFieldTypeList.get(j).getName().equals(name1)) {
                            NsiElementStringFieldType val = (NsiElementStringFieldType) nsiElementFieldTypeList.get(j);
                            insertNsi50(nsiElementType.getCode(), nsiElementType.getGUID(), String.valueOf(nsiElementType.getModified()), nsiElementType.isIsActual(), val.getValue());
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Error occurred when ExportNsiItemRequestS50");
            e.printStackTrace();
        }
    }

    //Сохраняем 27 или 2 справочник
    public static void ExportNsiItemRequestS27(List<NsiElementType> elementType, String registryNumber) {
        String tableName = null;
        if (registryNumber == "2") {
            tableName = "komres2";
        } else {
            if (registryNumber == "27") {
                tableName = "metdv27";
            } else {
                return;
            }
        }
        Connection conn = null;
        try {
            conn = database.dbfJDBCConnection();
        } catch (Exception e) {
            System.err.println("Error occurred when open connection");
            e.printStackTrace();
        }
        String sql = "";
        for (int i = 0; i < elementType.size(); i++) { //цикл по NsiElement в xml ответе
            NsiElementType nsiElementType = elementType.get(i);
            //достаем список элементов <NsiElementField>
            List<NsiElementFieldType> nsiElementFieldTypeList = nsiElementType.getNsiElementField();
            NsiElementStringFieldType val = (NsiElementStringFieldType) nsiElementFieldTypeList.get(0);


            //есть ли уже в таблице запись по полю code
            if (recordExist(conn, tableName, "code", nsiElementType.getCode(), false) > 0) {
                sql = "UPDATE " + tableName
                        + " SET guid = '" + nsiElementType.getGUID() + "',"
                        // +" modified = '" + nsiElementType.getModified() + "',"
                        + " isactual = " + nsiElementType.isIsActual() + ","
                        + " val = '" + val.getValue() + "'"
                        + " WHERE code = " + nsiElementType.getCode();

            } else {

                sql = "INSERT INTO " + tableName
                        + " (code,guid,modified,isactual,val) "
                        + " VALUES ("
                        + nsiElementType.getCode() + ",'" + nsiElementType.getGUID() + "',"
                        + "'" + nsiElementType.getModified() + "'," + nsiElementType.isIsActual()
                        + ",'" + val.getValue() + "'"
                        + ")";
            }
            database.updateSQL(conn, sql);
        }
        try {
            conn.close();
        } catch (Exception e) {
            System.err.println("Error occurred when close connection ");
            e.printStackTrace();
        }
    }

}
