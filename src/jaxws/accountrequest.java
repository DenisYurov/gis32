package jaxws;

import database.database;
import ru.gosuslugi.dom.schema.integration.base.HeaderType;
import ru.gosuslugi.dom.schema.integration.base.RequestHeader;
import ru.gosuslugi.dom.schema.integration.house_management.*;
import ru.gosuslugi.dom.schema.integration.house_management_service.HouseManagementPortsType;
import ru.gosuslugi.dom.schema.integration.house_management_service.HouseManagementService;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static database.database.recordExist;
import static jaxws.jaxutil.getGrigorian;
import static jaxws.jaxutil.logs;

/**
 * Created by dionis on 03.11.2016.
 */
public class accountrequest {
    public static Properties prop;

    //Получение списка лицевых счетов организации orgPPAGUID по дому fIASHouseGuid
    public static void exportAccountRequest(String orgPPAGUID, String fIASHouseGuid) {
        logs();//подключаем логгирование
        HouseManagementService service = new HouseManagementService();
        HouseManagementPortsType port = service.getHouseManagementPort();
        BindingProvider bp = (BindingProvider) port;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, prop.getProperty("ENDPOINT") + "/ext-bus-home-management-service/services/HomeManagement");
        //заголовок
        RequestHeader requestHeader = new RequestHeader();
        //поле с датой
        Date dt = new Date();
        XMLGregorianCalendar date2 = getGrigorian(dt);
        requestHeader.setDate(date2);
        requestHeader.setMessageGUID(java.util.UUID.randomUUID().toString());
        // закончили с датой
        requestHeader.setOrgPPAGUID(orgPPAGUID);
        requestHeader.setIsOperatorSignature(true);//признак оператора ИС
        Holder holder = new Holder<HeaderType>();
        ExportAccountRequest exportAccountRequest = new ExportAccountRequest();
        exportAccountRequest.setVersion("10.0.1.1");
        exportAccountRequest.setId("1");
        exportAccountRequest.setFIASHouseGuid(fIASHouseGuid);
        try {
            ExportAccountResult res = port.exportAccountData(exportAccountRequest, requestHeader, holder);
            exportAccountRequestSave(res, orgPPAGUID, fIASHouseGuid);
        } catch (Exception e) {
            System.err.println("Error occurred importPaymentDocumentData");
            e.printStackTrace();
        } ;
    }
    public static void exportAccountRequestSave(ExportAccountResult res, String orgPPAGUID, String fIASHouseGuid) {
        String tableName = "account";
        Connection conn = null;
        try {
            conn = database.dbfJDBCConnection();
        } catch (Exception e) {
            System.err.println("Error occurred connection"+tableName+".dbf");
            e.printStackTrace();
        }

        List<ExportAccountResultType> accountList = res.getAccounts();
        String sql = "DELETE FROM "+ tableName+" WHERE  orgppaguid = '"+orgPPAGUID+"' AND fiasguid = '"+fIASHouseGuid+"'";
        database.updateSQL(conn, sql);
        for (int i = 0; i < accountList.size(); i++) { //цикл по Accounts в xml ответе

            ExportAccountResultType account = accountList.get(i);
            //нет ли в базе такого лицевого счета
            //int dbRecordCount = recordExist(conn, tableName, "accguid", account.getAccountGUID(),true);
            //String sql;
            /*if (dbRecordCount > 0) {//в базе уже есть этот лицевой
                sql = "UPDATE " + tableName
                        + " SET accountnum = "+account.getAccountNumber()+","
                        + " unifiedacc = '" + account.getUnifiedAccountNumber()+"',"
                        + " serviceid = '" +account.getServiceID()+"',"
                        + " orgppaguid = '"+orgPPAGUID+"',"
                        + " fiasguid = '"+fIASHouseGuid+"',"
                        + " totalsquar = "+account.getTotalSquare()+","
                        + " residentsq = "+account.getResidentialSquare()
                        + " WHERE accguid = '" + account.getAccountGUID() + "'";

        } else { */
                sql = "INSERT INTO " + tableName
                        + "(accountnum,unifiedacc,serviceid,orgppaguid,fiasguid,accguid,totalsquar,residentsq)"
                        + " VALUES ("
                        +       account.getAccountNumber() + ","
                        + "'" + account.getUnifiedAccountNumber()+ "',"
                        + "'" + account.getServiceID() + "',"
                        + "'" + orgPPAGUID + "',"
                        + "'" + fIASHouseGuid + "',"
                        + "'" + account.getAccountGUID() + "',"
                        +       account.getTotalSquare() + ","
                        +       account.getResidentialSquare() + ")";

            database.updateSQL(conn, sql);
        }//цикл по списку лицевых
        try {
            conn.close();
        } catch (Exception e) {
            System.err.println("Error occurred close connection metdevt.dbf");
            e.printStackTrace();
        }
    }
}
