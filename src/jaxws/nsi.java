package jaxws;

import database.database;
import dataobject.nsi51DAO;
import dataobject.orgDAO;
import javafx.beans.property.SimpleDoubleProperty;
import ru.gosuslugi.dom.schema.integration.base.HeaderType;
import ru.gosuslugi.dom.schema.integration.base.RequestHeader;
import ru.gosuslugi.dom.schema.integration.nsi.ExportDataProviderNsiItemRequest;
import ru.gosuslugi.dom.schema.integration.nsi.ExportNsiItemResult;
import ru.gosuslugi.dom.schema.integration.nsi_base.*;
import ru.gosuslugi.dom.schema.integration.nsi_service.NsiPortsType;
import ru.gosuslugi.dom.schema.integration.nsi_service.NsiService;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static database.database.recordExist;
import static dataobject.nsi51DAO.deleteJilus51All;
import static jaxws.jaxutil.getGrigorian;
import static jaxws.jaxutil.logs;

/**
 * Created by dionis on 07.11.2016.
 */

public class nsi {
    public static Properties prop;
    public static volatile SimpleDoubleProperty progress;
    public static String orgPPAGUID;
    public static String registryNumber;

    public static SimpleDoubleProperty getProgress() {
        return progress;
    }

    // Вызов exportDataProviderNsiItemRequest для всех организаций из таблицы org
    public static void exportDataProviderNsiItemRequestAllOrg(String registryNumber,Boolean setDataProvider) {
        try {
            ResultSet rs = orgDAO.getAllOrg(setDataProvider);//выборка всех/только наших организаций
            // рассчет длины прогрессбара
            double cntOrg = (1.0 / orgDAO.getOrgRecCount(setDataProvider));
            deleteJilus51All();
            while (rs.next()) {
                exportDataProviderNsiItemRequest(rs.getString("orgppaguid"), registryNumber);
                progress.set(progress.get() + cntOrg);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on exportDataProviderNsiItemRequestAllOrg");
            progress.set(1);
        }

    }

    //Забираем справочник 1 или 51 и ГИС по организации orgPPAGUID
    public static void exportDataProviderNsiItemRequest(String orgPPAGUID, String registryNumber) {
        //logs();
        NsiService nService = new NsiService();
        NsiPortsType port = nService.getNsiPort();
        BindingProvider bp = (BindingProvider) port;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, prop.getProperty("ENDPOINT") + "/ext-bus-nsi-service/services/Nsi");
        //готовим данные для запроса
        //тело
        ExportDataProviderNsiItemRequest exportDataProviderNsiItemRequest = new ExportDataProviderNsiItemRequest();
        exportDataProviderNsiItemRequest.setVersion("10.0.1.2");
        exportDataProviderNsiItemRequest.setId("1");
        BigInteger nsiB = new BigInteger(registryNumber);
        exportDataProviderNsiItemRequest.setRegistryNumber(nsiB);
        //заголовок
        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setIsOperatorSignature(true);//признак оператора ИС
        //поле с датой
        Date dt = new Date();
        XMLGregorianCalendar date2 = getGrigorian(dt);
        requestHeader.setDate(date2);
        requestHeader.setMessageGUID(java.util.UUID.randomUUID().toString());
        requestHeader.setOrgPPAGUID(orgPPAGUID);
        // end date set
        Holder holder = new Holder<HeaderType>();
        //делаем запрос в ГИС и принимаем ответ.
        try {
            ExportNsiItemResult res = port.exportDataProviderNsiItem(exportDataProviderNsiItemRequest, requestHeader, holder);
            NsiItemType nsiItem = res.getNsiItem();
            //Запрос успешен, значит организация - наш dataprovider
            dataobject.orgDAO.setDataProvider(orgPPAGUID,true);
            //сохраняем
            if (registryNumber == "51") {
                exportDataProviderNsiItemSave51(nsiItem.getNsiElement(), orgPPAGUID);
            }
            if (registryNumber == "1") {
                exportDataProviderNsiItemSave1(nsiItem.getNsiElement(), orgPPAGUID);
            }

        } catch (Exception e) {
            System.err.println("Error occurred expportNsiListRequest");
            //помечаем организация не dataProvider на основе ошибки : AUT011009: Операция не разрешена
            if (e.getMessage().toLowerCase().contains("AUT011009".toLowerCase())) {
                dataobject.orgDAO.setDataProvider(orgPPAGUID,false);
            }
            e.printStackTrace();
        }
    }


    private static void exportDataProviderNsiItemSave51(List<NsiElementType> elementType, String orgPPAGUID) throws Exception {
        String tableName = "nsiloc51";
        Connection conn = database.dbfJDBCConnection();
        for (int i = 0; i < elementType.size(); i++) {
            NsiElementType nsiElementType = elementType.get(i);

            //есть ли уже в таблице запись по полю code
            //if (recordExist(conn, tableName, "guid + orgppaguid", nsiElementType.getGUID() + orgPPAGUID, true) > 0) {
            //    sql = "UPDATE " + tableName + " SET code = ?,  vcuregnum = ?, vcucode = ?, vcuguid = ?," +
            //            "nsirescode = ?, nsiresguid = ?, value = ?, orgppaguid = ?  WHERE guid = ?";
            //} else {
            String sql = "INSERT INTO " + tableName + "(code, vcuregnum, vcucode, vcuguid,  nsirescode, nsiresguid, value, orgppaguid, guid) VALUES(?,?,?,?,?,?,?,?,?)";
            //}

            PreparedStatement pstat = conn.prepareStatement(sql);
            pstat.setString(1, nsiElementType.getCode());
            //не актуальные пропускаем
            if (nsiElementType.isIsActual()) {
                List<NsiElementFieldType> nsiElementFieldTypeList = nsiElementType.getNsiElementField();
                //работаем с элементами списка NsiElementField
                pstat.setInt(2, 0);
                pstat.setString(3, null);
                pstat.setString(4, null);
                pstat.setString(7, null);
                pstat.setString(5, null);
                pstat.setString(6, null);
                for (int j = 0; j < nsiElementFieldTypeList.size(); j++) {

                    String src = "Вид коммунальной услуги";
                    String name1 = new String(src.getBytes("Cp1251"), "UTF-8");//без этого бреда не работает
                    if (nsiElementFieldTypeList.get(j).getName().equals(name1)) {
                        NsiElementNsiRefFieldType nsiElementNsiRefFieldType = (NsiElementNsiRefFieldType) nsiElementFieldTypeList.get(j);
                        NsiElementNsiRefFieldType.NsiRef nsiref = nsiElementNsiRefFieldType.getNsiRef();
                        pstat.setInt(2, nsiref.getNsiItemRegistryNumber().intValue());
                        pstat.setString(3, nsiref.getRef().getCode());
                        pstat.setString(4, nsiref.getRef().getGUID());
                    }
                    ;
                    src = "Главная коммунальная услуга";
                    name1 = new String(src.getBytes("Cp1251"), "UTF-8");
                    if (nsiElementFieldTypeList.get(j).getName().equals(name1)) {
                        NsiElementStringFieldType val = (NsiElementStringFieldType) nsiElementFieldTypeList.get(j);
                        pstat.setString(7, val.getValue());
                    }
                    ;
                    src = "Коммунальный ресурс";
                    name1 = new String(src.getBytes("Cp1251"), "UTF-8");
                    if (nsiElementFieldTypeList.get(j).getName().equals(name1)) {
                        NsiElementNsiRefFieldType nsiElementNsiRefFieldType = (NsiElementNsiRefFieldType) nsiElementFieldTypeList.get(j);
                        NsiElementNsiRefFieldType.NsiRef nsiref = nsiElementNsiRefFieldType.getNsiRef();
                        pstat.setString(5, nsiref.getRef().getCode());
                        pstat.setString(6, nsiref.getRef().getGUID());
                    }
                    ;
                }
                pstat.setString(8, orgPPAGUID);
                pstat.setString(9, nsiElementType.getGUID());
                pstat.executeUpdate();
            }
        }
        conn.close();

    }

    private static void exportDataProviderNsiItemSave1(List<NsiElementType> elementType, String orgPPAGUID) throws Exception {
        String tableName = "nsiloc1";

        Connection conn = database.dbfJDBCConnection();
        for (int i = 0; i < elementType.size(); i++) {
            NsiElementType nsiElementType = elementType.get(i);
            String sql = "";
            //есть ли уже в таблице запись по полю guid + orgppaguid
            if (recordExist(conn, tableName, "guid + orgppaguid", nsiElementType.getGUID() + orgPPAGUID, true) > 0) {
                sql = "UPDATE " + tableName + " SET code = ?, value = ?, orgppaguid = ?  WHERE guid = ?";
            } else {
                sql = "INSERT INTO " + tableName + "(code, value, orgppaguid, guid) VALUES(?,?,?,?)";
            }

            PreparedStatement pstat = conn.prepareStatement(sql);
            pstat.setString(1, nsiElementType.getCode());
            //не актуальные пропускаем
            if (nsiElementType.isIsActual()) {
                List<NsiElementFieldType> nsiElementFieldTypeList = nsiElementType.getNsiElementField();
                //работаем с элементами списка NsiElementField
                pstat.setString(2, null);
                for (int j = 0; j < nsiElementFieldTypeList.size(); j++) {

                    String src = "Вид дополнительной услуги";
                    String name1 = new String(src.getBytes("Cp1251"), "UTF-8");
                    if (nsiElementFieldTypeList.get(j).getName().equals(name1)) {
                        NsiElementStringFieldType val = (NsiElementStringFieldType) nsiElementFieldTypeList.get(j);
                        pstat.setString(2, val.getValue());
                    }

                }
                pstat.setString(3, orgPPAGUID);
                pstat.setString(4, nsiElementType.getGUID());
                pstat.executeUpdate();
            }
        }
        conn.close();

    }
}
