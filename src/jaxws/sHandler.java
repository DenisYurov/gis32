package jaxws;

import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Set;

import static signature.sign.signatureString;

/**
 * Created by dionis on 29.10.2016.
 * Перехватчик SOAP сообщений. Нужен для вставки подписи. Обрабатывает все сообщения сервиса к которому прикреплен.
 * <p>
 * В классе Service сгенерированном wsimport должно быть прикрепление перехватчика:
 *
 * @WebServiceClient(name = "HouseManagementService", targetNamespace = "http://dom.gosuslugi.ru/schema/integration/house-management-service/", wsdlLocation = "file:/C:/Users/dionis/IdeaProjects/hcs_wsdl_xsd_v.10.0.2.3/house-management/hcs-house-management-service.wsdl")
 * @HandlerChain(file="/jaxws/handler-chain.xml") public class HouseManagementService
 * так же должен существовать файл handler-chain.xml определяющий цепочку handler - ов
 */
public class sHandler implements SOAPHandler<SOAPMessageContext> {

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        //System.out.println("Client : handleMessage()......");
        Boolean isRequest = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        SOAPMessage soapMsg = context.getMessage();
        //if this is a request, true for outbound messages, false for inbound
        try {
            if (isRequest) {

                // SOAPMessage soapMsg = context.getMessage();
                soapMsg = signatureString(soapMsg);
                context.setMessage(soapMsg);
                //tracking
                //soapMsg.writeTo(System.out);

            }else{
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            Source sourceContent = soapMsg.getSOAPPart().getContent();
            StreamResult result = new StreamResult(new FileOutputStream("C:\\Users\\dionis\\IdeaProjects\\gis32\\logfile.xml", true /* append */));
            //StreamResult result = new StreamResult(System.out);
            transformer.transform(sourceContent, result);

        } catch (SOAPException e) {
            System.err.println(e);
        } catch (Exception e) {
            System.err.println("Error occurred in sHandler while signature made");
            e.printStackTrace();
            System.exit(4);
        }
        //continue other handler chain
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        System.out.println("Client : handleFault()......");
        return true;
    }

    @Override
    public void close(MessageContext context) {
        System.out.println("Client : close()......");
    }

    @Override
    public Set<QName> getHeaders() {
        System.out.println("Client : getHeaders()......");
        return null;
    }

}
