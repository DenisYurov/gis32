package soap.queries;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.Properties;

/**
 * Created by dionis on 13.10.2016.
 */
public class fictionresponse {
    //read xml from file
    public static Properties prop;

    public static Document documentFromFile(String file) {

        try {
            File fXmlFile = new File(file);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
