package soap.queries;

import database.database;
import org.w3c.dom.NodeList;

import javax.xml.soap.*;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Properties;

import static signature.sign.signatureString;

/**
 * Created by dionis on 13.10.2016.
 */
public class exportAccountRequest {
    public static Properties prop;
    //таблица где будут данные результата запроса
    public static String tableName = "account";
    public static void runQuery(String orgPPAGUID, String FIASHouseGuid) {
        try {
            SOAPMessage soapRequest = ExportAccountRequest(orgPPAGUID, FIASHouseGuid);
            soapRequest = signatureString(soapRequest);
            URL endpoint = new URL(prop.getProperty("ENDPOINT") + "/ext-bus-home-management-service/services/HomeManagement");
            SOAPMessage soapResponse = utility.soapConnection(soapRequest, endpoint);
            ArrayList<Account> komUsl3Array = exportNsiListResponse(soapResponse);
            saveToBase(komUsl3Array);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class Account {
        String isuoacco;
        String creatdate;
        String totalsquar;
        String residentsq;
        String premisesgu;
        String accountnum;
        String accountgui;
        String unifiedacc;
        String serviceid;
    }

    public static ArrayList<Account> exportNsiListResponse(SOAPMessage soapResponse) throws Exception {

        ArrayList<Account> accountArray = new ArrayList<Account>(); //строки справочника KomUsl3
        SOAPPart sp = soapResponse.getSOAPPart();
        SOAPEnvelope respEnv = sp.getEnvelope();
        SOAPBody respBody = respEnv.getBody();
        NodeList ns13_Accounts = respBody.getElementsByTagName("ns13:Accounts");
        for (int i = 0; i < ns13_Accounts.getLength(); i++) {
            Account account = new Account();
            SOAPElement ns13_Account = (SOAPElement) ns13_Accounts.item(i);
            SOAPElement ns13_isUOAccount = (SOAPElement) ns13_Account.getElementsByTagName("ns13:isUOAccount").item(0);
            account.isuoacco = ns13_isUOAccount.getValue();

            SOAPElement ns13_CreationDate = (SOAPElement) ns13_Account.getElementsByTagName("ns13:CreationDate").item(0);
            account.creatdate = ns13_CreationDate.getValue();

            NodeList ns13_TotalSquare = ns13_Account.getElementsByTagName("ns13:TotalSquare");
            if (ns13_TotalSquare.getLength() > 0) {
                account.totalsquar = ns13_TotalSquare.item(0).getFirstChild().getNodeValue();
            }

            NodeList ns13_ResidentialSquare = ns13_Account.getElementsByTagName("ns13:ResidentialSquare");
            if(ns13_ResidentialSquare.getLength()>0) {
                account.residentsq = ns13_ResidentialSquare.item(0).getFirstChild().getNodeValue();
            }
            SOAPElement ns13_PremisesGUID = (SOAPElement) ns13_Account.getElementsByTagName("ns13:PremisesGUID").item(0);
            account.premisesgu = ns13_PremisesGUID.getValue();
            SOAPElement ns5_AccountNumber = (SOAPElement) ns13_Account.getElementsByTagName("ns5:AccountNumber").item(0);
            account.accountnum = ns5_AccountNumber.getValue();
            SOAPElement ns13_AccountGUID = (SOAPElement) ns13_Account.getElementsByTagName("ns13:AccountGUID").item(0);
            account.accountgui = ns13_AccountGUID.getValue();
            SOAPElement ns5_UnifiedAccountNumber = (SOAPElement) ns13_Account.getElementsByTagName("ns5:UnifiedAccountNumber").item(0);
            account.unifiedacc = ns5_UnifiedAccountNumber.getValue();
            SOAPElement ns5_ServiceID = (SOAPElement) ns13_Account.getElementsByTagName("ns5:ServiceID").item(0);
            account.serviceid = ns5_ServiceID.getValue();
            accountArray.add(account);
            account = null;
        }
        return accountArray;
    }

    private static SOAPMessage ExportAccountRequest(String orgPPAGUID, String FIASHouseGuid) {
        try {
            SOAPMessage soapMessage = utility.domToSoapMessage(fictionresponse.documentFromFile("C:\\Users\\dionis\\IdeaProjects\\gis32\\xml\\template\\exportAccountReguest.xml")); //получение SOAP ответа из файла
            //вставляем нужные узлы
            SOAPElement header = soapMessage.getSOAPHeader();
            SOAPElement base_MessageGUID = (SOAPElement) header.getElementsByTagName("base:MessageGUID").item(0);//.getFirstChild();
            base_MessageGUID.setValue(java.util.UUID.randomUUID().toString());
            SOAPElement base_orgPPAGUID = (SOAPElement) header.getElementsByTagName("base:orgPPAGUID").item(0);
            base_orgPPAGUID.setValue(orgPPAGUID);
            SOAPElement body = soapMessage.getSOAPBody();
            SOAPElement hous_FIASHouseGuid = (SOAPElement) body.getElementsByTagName("hous:FIASHouseGuid").item(0);
            hous_FIASHouseGuid.setValue(FIASHouseGuid);
            //MimeHeaders
            MimeHeaders headers = soapMessage.getMimeHeaders();
            headers.addHeader("SOAPAction", "urn:exportAccountData");
            soapMessage.saveChanges();
            utility.printSOAPMessage(soapMessage);
            return soapMessage;
        } catch (Exception e) {
            System.err.println("Error occurred while reading SOAPtemplate from file");
            e.printStackTrace();
            return null;
        }
    }
    private static void saveToBase(ArrayList<Account> accountArray) throws Exception {
        Connection conn = database.dbfConnection();
        String sql = "delete from "+tableName;
        database.executeSQL(conn, sql);
        conn.close();
        conn = database.dbfConnection();
        sql = "PACK "+tableName;
        database.executeSQL(conn, sql);
        conn.close();
        conn = null;
        conn = database.dbfJDBCConnection();
        for (int i = 0; i < accountArray.size(); i++) {
            Account el = accountArray.get(i);
            sql = "INSERT INTO "+tableName
                    +" (isuoacco,creatdate,totalsquar,residentsq,premisesgu,accountnum,accountgui,unifiedacc,serviceid) "
                    +" VALUES ("
                    + el.isuoacco + ",'" + el.creatdate + "',"
                    + el.totalsquar + "," + el.residentsq + ",'"
                    + el.premisesgu + "'," + el.accountnum + ",'"
                    + el.accountgui + "','" + el.unifiedacc + "','"
                    + el.serviceid
                    + "')";
            database.updateSQL(conn, sql);
        }
        conn.close();
    }
}
