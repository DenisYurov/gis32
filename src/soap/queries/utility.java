package soap.queries;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.soap.*;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.net.URL;
import java.util.Properties;

/**
 * Created by dionis on 13.10.2016.
 */
public class utility {
    public static Properties prop;

    public static SOAPMessage domToSoapMessage(Document document) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        DOMSource domSource = new DOMSource(document);
        soapPart.setContent(domSource);
        return soapMessage;
    }

    /**
     * Method used to print the SOAP Response
     */
    public static void printSOAPMessage(SOAPMessage soapResponse) throws Exception {

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        Source sourceContent = soapResponse.getSOAPPart().getContent();
        System.out.print("\nResponse SOAP Message = ");
        StreamResult result = new StreamResult(System.out);
        transformer.transform(sourceContent, result);

    }

    //stripControlSymbols
    public static String stripControlChars(String iString) {
        StringBuffer result = new StringBuffer(iString);
        int idx = result.length();
        while (idx-- > 0) {
            if (result.charAt(idx) < 0x20 && result.charAt(idx) != 0x9 &&
                    result.charAt(idx) != 0xA && result.charAt(idx) != 0xD) {
                System.out.print("deleted character at: " + idx);

                result.deleteCharAt(idx);
            }
        }
        return result.toString();
    }

    //trim space text nodes
    public static void trimWhitespace(Node node) {
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); ++i) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.TEXT_NODE) {

                child.setTextContent(child.getTextContent().trim());
                if (child.getTextContent().length() == 0) {
                    node.removeChild(child);
                }
            }
            trimWhitespace(child);
        }
    }

    public static SOAPMessage soapConnection(SOAPMessage soapMessage,URL endpoint) {
        if (prop.getProperty("soapRespFromServer").equals("true")) {//реальный ответ или фейк
            //отправка реального SOAP на сервер, чтение ответа
            try {
                // Create SOAP Connection
                SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
                SOAPConnection soapConnection = soapConnectionFactory.createConnection();
                // Send SOAP Message to SOAP Server
                //URL endpoint = new URL(prop.getProperty("ENDPOINT"));
                SOAPMessage soapResponse = soapConnection.call(soapMessage, endpoint);
                soapConnection.close();
                printSOAPMessage(soapResponse);
                return soapResponse;
            } catch (Exception e) {
                System.err.println("Error occurred while sending SOAP Request to Server");
                e.printStackTrace();
                return null;
            }

        } else {//фейковый ответ берем из файла
            try {
                SOAPMessage soapResponse = utility.domToSoapMessage(fictionresponse.documentFromFile(prop.getProperty("xmlTestResponse"))); //получение SOAP ответа из файла
                printSOAPMessage(soapResponse);
                return soapResponse;
            } catch (Exception e) {
                System.err.println("Error occurred while reading SOAPresponse from file");
                e.printStackTrace();
                return null;
            }
        }
    }
}
