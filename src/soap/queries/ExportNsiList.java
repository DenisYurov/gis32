package soap.queries;
import database.database;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import javax.xml.soap.*;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by dionis on 13.10.2016.
 * Запрос справочника коммунальных услуг. Сохранение в базу
 */
public class ExportNsiList {
    //таблица где будут данные результата запроса
    public static String tableName = "komusl";
    public static Properties prop;
    //объект для хранения строки справочника KomUsl3. названия полей совпадают с названиями полей таблицы
    public static class KomUsl3 {
        String code;
        String guid;
        String value;
        String isActual;
        String modified;
        String edizm_code;
        String edizmprna;
        String nsiRefNum;
        String nsiRefn_cd;
        String nsirfn_gui;
    }

    public static void runQuery() {
        try {
            SOAPMessage soapRequest = ExportNsiListRequest("3");
            URL endpoint = new URL(prop.getProperty("ENDPOINT"+"/ext-bus-nsi-common-service/services/NsiCommon"));
            SOAPMessage soapResponse = utility.soapConnection(soapRequest,endpoint);
            ArrayList<KomUsl3> komUsl3Array = exportNsiListResponse(soapResponse);
            saveToBase(komUsl3Array);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void saveToBase(ArrayList<KomUsl3> komUsl3Array) throws Exception {
        Connection conn = database.dbfConnection();
        String sql = "delete from komusl";
        database.executeSQL(conn, sql);
        conn.close();
        conn = database.dbfConnection();
        sql = "PACK komusl";
        database.executeSQL(conn, sql);
        conn.close();
        conn = null;
        conn = database.dbfJDBCConnection();
        for (int i = 0; i < komUsl3Array.size(); i++) {
            KomUsl3 el = komUsl3Array.get(i);
            sql = "INSERT INTO komusl (code,guid,val,isactual,modified,edizm_code,edizmprna,nsirefnum,nsiRefn_cd,nsirfn_gui) VALUES ("
                    + el.code + ",'" + el.guid + "','"
                    + el.value + "'," + el.isActual + ",'"
                    + el.modified + "'," + el.edizm_code + ","
                    + el.edizmprna + "," + el.nsiRefNum + ","
                    + el.nsiRefn_cd + ",'" + el.nsirfn_gui
                    + "')";
            database.updateSQL(conn, sql);
        }
        conn.close();
    }

    private static SOAPMessage ExportNsiListRequest(String registryNumber) throws Exception {
        /*
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:base="http://dom.gosuslugi.ru/schema/integration/base/" xmlns:nsi="http://dom.gosuslugi.ru/schema/integration/nsi-common/" xmlns:nsi1="http://dom.gosuslugi.ru/schema/integration/nsi-base/" xmlns:xd="http://www.w3.org/2000/09/xmldsig#">
        <SOAP-ENV:Header>
        <base:ISRequestHeader><base:Date>2016-10-09T23:23:00</base:Date>
        <base:MessageGUID>89418cf0-44e9-4cb7-ac3a-c70c5569f7fb
        </base:MessageGUID></base:ISRequestHeader></SOAP-ENV:Header>
        <SOAP-ENV:Body><nsi:exportNsiItemRequest base:version="10.0.1.2">
        <nsi:RegistryNumber>3</nsi:RegistryNumber>
        <nsi1:ListGroup>NSI</nsi1:ListGroup></nsi:exportNsiItemRequest>
        </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
        */

        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String nameURI1 = "http://dom.gosuslugi.ru/schema/integration/base/";
        String nameURI2 = "http://dom.gosuslugi.ru/schema/integration/nsi-common/";
        String nameURI3 = "http://www.w3.org/2000/09/xmldsig#";
        String nameURI4 = "http://dom.gosuslugi.ru/schema/integration/nsi-base/";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("base", nameURI1);
        envelope.addNamespaceDeclaration("nsi", nameURI2);
        envelope.addNamespaceDeclaration("xd", nameURI3);
        envelope.addNamespaceDeclaration("nsi1", nameURI4);

        //SOAP Header
        SOAPHeader soapHeader = envelope.getHeader();
        SOAPElement soapHeaderElem = soapHeader.addChildElement("ISRequestHeader", "base");
        SOAPElement soapHeaderElem1 = soapHeaderElem.addChildElement("Date", "base");
        soapHeaderElem1.addTextNode("2016-10-09T23:23:00");
        SOAPElement soapHeaderElem2 = soapHeaderElem.addChildElement("MessageGUID", "base");
        soapHeaderElem2.addTextNode("89418cf0-44e9-4cb7-ac3a-c70c5569f7fb");
        //soapHeaderElem2.addTextNode("${=java.util.UUID.randomUUID()}");

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("exportNsiItemRequest", "nsi");
        soapBodyElem.addAttribute(envelope.createQName("version", "base"), "10.0.1.2");
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("RegistryNumber", "nsi");
        soapBodyElem1.addTextNode(registryNumber); //Из параметров функции
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("ListGroup", "nsi1");
        soapBodyElem2.addTextNode("NSI");
        //MimeHeaders
        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", "urn:exportNsiItem");

        soapMessage.saveChanges();
        /* Print the request message */

        System.out.print("Request SOAP Message = ");
        soapMessage.writeTo(System.out);
        System.out.println();

        return soapMessage;
    }

    public static ArrayList<KomUsl3> exportNsiListResponse(SOAPMessage soapResponse) throws Exception {

        ArrayList<KomUsl3> komUsl3Array = new ArrayList<KomUsl3>(); //строки справочника KomUsl3

        SOAPPart sp = soapResponse.getSOAPPart();
        SOAPEnvelope respEnv = sp.getEnvelope();
        SOAPBody respBody = respEnv.getBody();
        //NodeList se = respBody.getElementsByTagName("ns13:exportNsiItemResult");
        SOAPElement ns13_exportNsiItemResult = (SOAPElement) respBody
                .getElementsByTagName("ns13:exportNsiItemResult")
                .item(0);
        SOAPElement ns13_NsiItem = (SOAPElement) ns13_exportNsiItemResult
                .getElementsByTagName("ns13:NsiItem")
                .item(0);
        NodeList ns6_NsiElements = ns13_NsiItem.getElementsByTagName("ns6:NsiElement");//Список видов коммунальной услуги
        Document doc;

        for (int i = 0; i < ns6_NsiElements.getLength(); i++) {
            KomUsl3 komUsl3Rec = new KomUsl3();//строка справочника
            SOAPElement ns6_NsiElement_i = (SOAPElement) ns6_NsiElements.item(i);
            komUsl3Rec.code = ns6_NsiElement_i.getElementsByTagName("ns6:Code").item(0).getFirstChild().getNodeValue();//текст это тоже Node
            komUsl3Rec.guid = ns6_NsiElement_i.getElementsByTagName("ns6:GUID").item(0).getFirstChild().getNodeValue();
            komUsl3Rec.modified = ns6_NsiElement_i.getElementsByTagName("ns6:Modified").item(0).getFirstChild().getNodeValue();
            komUsl3Rec.isActual = ns6_NsiElement_i.getElementsByTagName("ns6:IsActual").item(0).getFirstChild().getNodeValue();
            NodeList ns6_NsiElementFields = ns6_NsiElement_i.getElementsByTagName("ns6:NsiElementField");
            for (int j = 0; j < ns6_NsiElementFields.getLength(); j++) {
                SOAPElement ns6_NsiElementField = (SOAPElement) ns6_NsiElementFields.item(j);
                SOAPElement ns6_Name = (SOAPElement) ns6_NsiElementField.getElementsByTagName("ns6:Name").item(0);
                String ns6_NameTxt = ns6_Name.getFirstChild().getNodeValue();
                if (ns6_NameTxt.equals("Вид коммунальной услуги")) {
                    komUsl3Rec.value = ns6_NsiElementField.getElementsByTagName("ns6:Value")
                            .item(0)
                            .getFirstChild()
                            .getNodeValue();
                }
                ;
                if (ns6_NameTxt.equals("Единица измерения")) {
                    komUsl3Rec.edizm_code = ns6_NsiElementField.getElementsByTagName("ns6:Code")
                            .item(0)
                            .getFirstChild()
                            .getNodeValue();
                }

                if (ns6_NameTxt.equals("Единица измерения мощности и присоединенной нагрузки")) {
                    if (ns6_NsiElementField.getElementsByTagName("ns6:Code").getLength() > 0) {
                        komUsl3Rec.edizm_code = ns6_NsiElementField.getElementsByTagName("ns6:Code")
                                .item(0)
                                .getFirstChild()
                                .getNodeValue();
                    }
                }

                if (ns6_NameTxt.equals("Вид коммунального ресурса для ОКИ")) {

                    SOAPElement ns6_NsiRef = (SOAPElement) ns6_NsiElementField.getElementsByTagName("ns6:NsiRef").item(0);
                    if (ns6_NsiRef != null && ns6_NsiRef.getElementsByTagName("ns6:NsiItemRegistryNumber").getLength() > 0) {
                        SOAPElement ns6_NsiItemRegistryNumber = (SOAPElement) ns6_NsiRef.getElementsByTagName("ns6:NsiItemRegistryNumber").item(0);
                        komUsl3Rec.nsiRefNum = ns6_NsiItemRegistryNumber.getFirstChild()
                                .getNodeValue();
                        SOAPElement ns6_Ref = (SOAPElement) ns6_NsiRef.getElementsByTagName("ns6:Ref").item(0);
                        komUsl3Rec.nsiRefn_cd = ns6_Ref.getElementsByTagName("ns6:Code").item(0).getFirstChild()
                                .getNodeValue();
                        komUsl3Rec.nsirfn_gui = ns6_Ref.getElementsByTagName("ns6:GUID").item(0).getFirstChild()
                                .getNodeValue();
                    }

                }

            }

            //String MyValue = new String(komUsl3Rec.value.getBytes("utf-8"),"cp866");
            // Array[] b = komUsl3Rec.value.getBytes( "Cp1251" );
            komUsl3Array.add(komUsl3Rec);
            komUsl3Rec = null;
        }
        return komUsl3Array;
    }

}
