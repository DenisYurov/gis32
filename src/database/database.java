package database;

import java.sql.*;
import java.sql.Date;
import java.util.*;

/**
 * Created by dionis on 13.10.2016.
 */

public class database {

    public static Connection conn;
    public static Connection conndos;
    public static Properties prop;
    public static Connection dbfJDBCConnection() throws ClassNotFoundException, SQLException {
        String dbfPath = prop.getProperty("dbfPath")+"?charSet=cp1251";
        Class.forName("com.hxtt.sql.dbf.DBFDriver");
        conn = DriverManager.getConnection("jdbc:dbf:/" + dbfPath);
        return conn;
    }
    public static Connection dbfJDBCConnectionDos() throws ClassNotFoundException, SQLException {
        String dbfPath = prop.getProperty("dbfPath")+"?charSet=cp866";
        Class.forName("com.hxtt.sql.dbf.DBFDriver");
        conndos = DriverManager.getConnection("jdbc:dbf:/" + dbfPath);
        return conndos;
    }

    public static Connection dbfJDBCConnPath(String path) throws ClassNotFoundException, SQLException {
        String dbfPath = prop.getProperty("dbfPath")+path+"?charSet=cp1251";
        Class.forName("com.hxtt.sql.dbf.DBFDriver");
        Connection connPath = DriverManager.getConnection("jdbc:dbf:/" + dbfPath);
        return connPath;

    }
    public static Connection dbfConnection() {
        try {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            //String connString="jdbc:odbc:Driver={Microsoft Visual FoxPro Driver};SourceType=DBF;SourceDB=C:\\Users\\dionis\\IdeaProjects\\gis32\\dbf;Exclusive=No;?charSet=cp1251";
            String connString="jdbc:odbc:Driver={Microsoft Visual FoxPro Driver};SourceType=DBF;SourceDB="+prop.getProperty("dbfPath")+";Exclusive=No;?charSet=cp1251";
            //String connString = "jdbc:odbc:DRIVER={Microsoft dBASE Driver (*.dbf)};DriverID=277;Dbq=C:\\Users\\dionis\\IdeaProjects\\gis32\\dbf?charSet=cp1251";
            Connection connection = DriverManager.getConnection(connString);
            return connection;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void updateSQL(Connection conn, String sql) {
        System.out.println(sql);
        Statement stmt = null;
        /*
        try {
            PreparedStatement pstmt =
                    conn.prepareStatement(sql);
            pstmt.setArray(1,ar);
            pstmt.executeUpdate();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        */

        try {

            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

    }
    public static ResultSet executeSQL(Connection conn, String sql) {

        Statement stmt = null;
        ResultSet rs = null;
        System.out.println(sql);
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            stmt.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        return rs;

    }

    public static Connection connPostgre() {
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(prop.getProperty("postgresurl"), prop.getProperty("postgresuser"), prop.getProperty("postgrespassword"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(2);
        }
        return conn;
    }
    //возвращает число записей в таблице tableName если fieldName = val
    public static int recordExist(Connection conn,String tableName,String fieldName, String val, Boolean valIsChar){
        String sqlAddon ="";
        if (valIsChar) {
            sqlAddon = "'";
        }
        String sql = "SELECT COUNT(*) AS cnt FROM "+tableName+" WHERE "+fieldName+" = "+sqlAddon+val+sqlAddon;
        int res = 0;
        ResultSet rs = database.executeSQL(conn,sql);
        try {
            rs.next();
            res = rs.getInt("cnt");
            //System.out.println( count);
        }
        catch (SQLException e) {
            e.printStackTrace();
            System.exit(2);
        }
        return res;
    }

    public static String getGGGGNow(int delta) {
        java.util.Date date = new java.util.Date(); // your date
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, delta);
        String year = Integer.toString(cal.get(Calendar.YEAR));
        return year;
    }
    public static String getMMNow(int delta) {
        java.util.Date date = new java.util.Date(); // your date
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, delta);
        String month = Integer.toString(cal.get(Calendar.MONTH)+1);//month is 0 based
        return month;

    }
}
