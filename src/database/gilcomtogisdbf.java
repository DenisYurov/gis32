package database;

import java.sql.*;

import dataobject.accountDAO;
import dataobject.liceDAO;
import dataobject.municservDAO;
import javafx.beans.property.SimpleDoubleProperty;
import org.omg.CORBA.portable.ApplicationException;
import ui.model.AccountModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import static database.database.getGGGGNow;
import static database.database.getMMNow;
import static dataobject.accgilcomerrDAO.deleteAccGilcomErr;
import static dataobject.accgilcomerrDAO.insertAccGilcomErr;
import static dataobject.accountDAO.getAccount;
import static dataobject.accountDAO.getAccountCount;
import static dataobject.billdocDAO.deleteAllBill;
import static dataobject.billdocDAO.deleteBill;
import static dataobject.billdocDAO.insertBill;
import static dataobject.billsoaplogDAO.deleteAllBillSoapLog;
import static dataobject.gisDAO.*;
import static dataobject.jilservDAO.*;
import static dataobject.liceDAO.getLiceByAccount;
import static dataobject.municservDAO.*;
import static dataobject.nsi51DAO.getNsi51Item1;
import static java.lang.Math.abs;
import static jaxws.nsi.orgPPAGUID;

/**
 * Created by dionis on 05.12.2016.
 */
public class gilcomtogisdbf {
    public static Properties prop;
    private static double progressDelta;
    //положение прогрессбара
    public static volatile SimpleDoubleProperty progress;

    public static void moveData() throws Exception {


        //вычисляем дельту прогрессбара
        progressDelta = (1.0 / getAccountCount());
        ResultSet accountRS = getAccount();

        deleteAllBill();
        deleteAllJilServ();
        deleteAllMunicServ();
        deleteAccGilcomErr();
        database.dbfJDBCConnection();//обновляем соединение
        //передаем в модули работы с базой mmgg прошлый месяц
        liceDAO.mmggMonthBack = getMMNow(-1) + getGGGGNow(-1).substring(2);
        municservDAO.mmggMonthBack = getMMNow(-1) + getGGGGNow(-1).substring(2);
        ResultSet jilusSodRS = getJilUs50(1);//Плата за содержание и ремонт помещения. code = 1
        jilusSodRS.next();
        ResultSet jilusNaemRS = getJilUs50(9);//Плата за наем. code = 9
        jilusNaemRS.next();
        ResultSet jilusKapremRS = getJilUs50(2);//Взнос за капремонт. code = 2
        jilusKapremRS.next();
        while (accountRS.next()) {
            String accountnum = accountRS.getString("accountnum");
            ResultSet accLiceRs = getGisByAccount(accountnum);//таблица gilcom.gismmgg

            while (accLiceRs.next()) {
                //причины перерассчета
                String recalcreas1 ="";
                String recalcreas2 ="";
                try {
                    insertBill(accountRS.getString("orgppaguid"), accountRS.getString("accguid"), accLiceRs.getInt("kollic"), abs(accLiceRs.getDouble("skt")), accLiceRs.getDouble("solic"));
                    //жилищная услуга
                    //содержание
                    if (accLiceRs.getDouble("zam1") != 0) {
                        insertJilServ(accountRS.getString("accguid"), 1, jilusSodRS.getString("guid"), accLiceRs.getDouble("tusl1"), accLiceRs.getDouble("zam1"), accLiceRs.getDouble("kopl1"), accLiceRs.getDouble("solic"), 0.0, "", accLiceRs.getDouble("k1"));
                    }
                    //наем
                    if (accLiceRs.getDouble("naem") != 0) {
                        insertJilServ(accountRS.getString("accguid"), 9, jilusNaemRS.getString("guid"), accLiceRs.getDouble("tusl10"), accLiceRs.getDouble("naem"), accLiceRs.getDouble("kopl10"), accLiceRs.getDouble("solic"), 0.0, "", accLiceRs.getDouble("k10"));
                    }
                    //капитальный ремонт
/*
                    if (accLiceRs.getDouble("kaprem") != 0) {
                        insertJilServ(accountRS.getString("accguid"), 2, jilusKapremRS.getString("guid"), accLiceRs.getDouble("tusl11"), accLiceRs.getDouble("kaprem"), accLiceRs.getDouble("kopl11"), accLiceRs.getDouble("solic"), 0.0, "", accLiceRs.getDouble("k11"));
                    }
*/
                    //коммунальная услуга
                    //отопление
                    if (accLiceRs.getDouble("zatepl") != 0) {
                        String src = "Отопление";//по этой строке будет поиск в справочнике 51
                        src = new String(src.getBytes("Cp1251"), "UTF-8");
                        ResultSet nsi51ItemRS = getNsi51Item1(accountRS.getString("orgppaguid"), src);
                        //Есть ли запись в индивидуальном справочнике 51
                        if (nsi51ItemRS.next()) {
                            if (accLiceRs.getDouble("kopl8") != 0) {
                                municservDAO.initialize();
                                municservDAO.accoguid = accountRS.getString("accguid");
                                municservDAO.nsicode = nsi51ItemRS.getString("code");
                                municservDAO.nsiguid = nsi51ItemRS.getString("guid");
                                municservDAO.rate = accLiceRs.getDouble("tusl8");
                                municservDAO.acctotal = accLiceRs.getDouble("zatepl");
                                municservDAO.totalpay = accLiceRs.getDouble("kopl8");
                                municservDAO.recalc = accLiceRs.getDouble("k8");
                                municservDAO.recalcreas = accLiceRs.getString("pr8");
                                municservDAO.recalcsum = Math.abs(accLiceRs.getDouble("k8"));
                                municservDAO.cnsindval = accLiceRs.getDouble("o8");
                                municservDAO.insertServ();
                            }
                        } else {
                            throw new MyOwnException("Otopl service in nsi51 not found");
                        }
                    }
                    //холодная вода ИП + ОДН
                    if (accLiceRs.getDouble("vod") != 0 || accLiceRs.getDouble("d_vod") != 0) {
                        String src = "Холодное водоснабжение";//по этой строке будет поиск в справочнике 51
                        src = new String(src.getBytes("Cp1251"), "UTF-8");
                        ResultSet nsi51ItemRS = getNsi51Item1(accountRS.getString("orgppaguid"), src);
                        //Есть ли запись в индивидуальном справочнике 51
                        if (nsi51ItemRS.next()) {

                            municservDAO.initialize();
                            municservDAO.accoguid = accountRS.getString("accguid");
                            municservDAO.nsicode = nsi51ItemRS.getString("code");
                            municservDAO.nsiguid = nsi51ItemRS.getString("guid");
                            municservDAO.acctotal = accLiceRs.getDouble("vod") + accLiceRs.getDouble("d_vod");
                            municservDAO.totalpay = accLiceRs.getDouble("kopl5") + accLiceRs.getDouble("kopl15");
                            //if(municservDAO.totalpay <0){municservDAO.totalpay = 0.0;}//гис не принимает "-" в этом поле
                            municservDAO.rate = accLiceRs.getDouble("tusl5");
                            municservDAO.recalc = accLiceRs.getDouble("k5") + accLiceRs.getDouble("k15");
                            recalcreas1 ="";
                            if (accLiceRs.getString("pr5") != null){
                                recalcreas1 = accLiceRs.getString("pr5");
                            }
                            recalcreas2 ="";
                            if (accLiceRs.getString("pr15") != null){
                                recalcreas2 = accLiceRs.getString("pr15");
                            }
                            municservDAO.recalcreas = recalcreas1 + " " + recalcreas2;
                            municservDAO.recalcsum = Math.abs(accLiceRs.getDouble("k5") + accLiceRs.getDouble("k15"));
                            municservDAO.cnsindmetd = accLiceRs.getString("mt5");
                            municservDAO.cnsobhmetd = accLiceRs.getString("mt15");
                            municservDAO.cnsindval = accLiceRs.getDouble("o5");
                            municservDAO.cnsobshval = accLiceRs.getDouble("o15");
                            //Текущие показания приборов учёта коммунальных услуг - индивидульное потребление
                            municservDAO.sinfindval = accLiceRs.getDouble("hvod");
                            //Текущие показания приборов учёта коммунальных услуг - общедомовые нужды
                            municservDAO.sinfhonval = accLiceRs.getDouble("hv");
                            //Суммарный объём коммунальных услуг в доме - индивидульное потребление
                            municservDAO.sinfhotin = accLiceRs.getDouble("sd5");
                            //Суммарный объём коммунальных услуг в доме - общедомовые нужды
                            municservDAO.sinfhotnee = accLiceRs.getDouble("odn5");
                            //Норматив потребления коммунальных услуг - индивидульное потребление
                            municservDAO.sinfincnor = accLiceRs.getDouble("norm5");
                            //Норматив потребления коммунальных услуг - общедомовые нужды
                            municservDAO.sinfhonnr = accLiceRs.getDouble("norm15");
                            municservDAO.insertServ();

                        } else {
                            throw new MyOwnException("Xol Vod service in nsi51 not found");
                        }
                    }

                    //горячая вода ИП+ODN
                    if (accLiceRs.getDouble("gor") != 0 || accLiceRs.getDouble("d_gor") != 0) {
                        String src = "Горячее водоснабжение";//по этой строке будет поиск в справочнике 51
                        src = new String(src.getBytes("Cp1251"), "UTF-8");
                        ResultSet nsi51ItemRS = getNsi51Item1(accountRS.getString("orgppaguid"), src);
                        //Есть ли запись в индивидуальном справочнике 51
                        if (nsi51ItemRS.next()) {
                            municservDAO.initialize();
                            municservDAO.accoguid = accountRS.getString("accguid");
                            municservDAO.nsicode = nsi51ItemRS.getString("code");
                            municservDAO.nsiguid = nsi51ItemRS.getString("guid");
                            municservDAO.acctotal = accLiceRs.getDouble("gor") + accLiceRs.getDouble("d_gor");
                            municservDAO.totalpay = accLiceRs.getDouble("kopl6") + accLiceRs.getDouble("kopl16");
                            municservDAO.rate = accLiceRs.getDouble("tusl6");
                            municservDAO.recalc = accLiceRs.getDouble("k6") + accLiceRs.getDouble("k16");
                            recalcreas1 ="";
                            if (accLiceRs.getString("pr6") != null){
                                recalcreas1 = accLiceRs.getString("pr6");
                            }
                            recalcreas2 ="";
                            if (accLiceRs.getString("pr16") != null){
                                recalcreas2 = accLiceRs.getString("pr16");
                            }
                            municservDAO.recalcreas = recalcreas1 + " " + recalcreas2;
                            municservDAO.recalcsum = Math.abs(accLiceRs.getDouble("k6") + accLiceRs.getDouble("k16"));
                            municservDAO.cnsindmetd = accLiceRs.getString("mt6");
                            municservDAO.cnsobhmetd = accLiceRs.getString("mt16");
                            municservDAO.cnsindval = accLiceRs.getDouble("o6");
                            municservDAO.cnsobshval = accLiceRs.getDouble("o16");
                            //Текущие показания приборов учёта коммунальных услуг - индивидульное потребление
                            municservDAO.sinfindval = accLiceRs.getDouble("gvod");
                            //Текущие показания приборов учёта коммунальных услуг - общедомовые нужды
                            municservDAO.sinfhonval = accLiceRs.getDouble("gv");
                            //Суммарный объём коммунальных услуг в доме - индивидульное потребление
                            municservDAO.sinfhotin = accLiceRs.getDouble("sd6");
                            //Суммарный объём коммунальных услуг в доме - общедомовые нужды
                            municservDAO.sinfhotnee = accLiceRs.getDouble("odn6");
                            //Норматив потребления коммунальных услуг - индивидульное потребление
                            municservDAO.sinfincnor = accLiceRs.getDouble("norm6");
                            //Норматив потребления коммунальных услуг - общедомовые нужды
                            municservDAO.sinfhonnr = accLiceRs.getDouble("norm16");
                            municservDAO.insertServ();
                        } else {
                            throw new MyOwnException("Gor Vod service in nsi51 not found");
                        }
                    }

                    //водоотведение
                    if (accLiceRs.getDouble("otvod") != 0) {
                        String src = "Водоотведение";//по этой строке будет поиск в справочнике 51
                        src = new String(src.getBytes("Cp1251"), "UTF-8");
                        ResultSet nsi51ItemRS = getNsi51Item1(accountRS.getString("orgppaguid"), src);
                        //Есть ли запись в индивидуальном справочнике 51
                        if (nsi51ItemRS.next()) {
                            municservDAO.initialize();
                            municservDAO.accoguid = accountRS.getString("accguid");
                            municservDAO.nsicode = nsi51ItemRS.getString("code");
                            municservDAO.nsiguid = nsi51ItemRS.getString("guid");
                            municservDAO.acctotal = accLiceRs.getDouble("otvod");
                            municservDAO.totalpay = accLiceRs.getDouble("kopl14");
                            municservDAO.rate = accLiceRs.getDouble("tusl14");
                            municservDAO.recalc = accLiceRs.getDouble("k14");
                            municservDAO.recalcreas = accLiceRs.getString("pr14");
                            municservDAO.recalcsum = Math.abs(accLiceRs.getDouble("k14"));
                            municservDAO.cnsindmetd = "O";
                            municservDAO.cnsindval = accLiceRs.getDouble("o14");

                            municservDAO.insertServ();
                        } else {
                            throw new MyOwnException("Otvod service in nsi51 not found");
                        }
                    }
                    //электроэнергия ИП+ ОДН день
                    if (accLiceRs.getDouble("energ") != 0 || accLiceRs.getDouble("d_energ") != 0) {
                        String src = "Электроснабжение";//по этой строке будет поиск в справочнике 51
                        src = new String(src.getBytes("Cp1251"), "UTF-8");
                        ResultSet nsi51ItemRS = getNsi51Item1(accountRS.getString("orgppaguid"), src);
                        //Есть ли запись в индивидуальном справочнике 51
                        if (nsi51ItemRS.next()) {
                            municservDAO.initialize();
                            municservDAO.accoguid = accountRS.getString("accguid");
                            municservDAO.nsicode = nsi51ItemRS.getString("code");
                            municservDAO.nsiguid = nsi51ItemRS.getString("guid");
                            municservDAO.acctotal = accLiceRs.getDouble("energ") + accLiceRs.getDouble("d_energ");
                            municservDAO.totalpay = accLiceRs.getDouble("kopl9") + accLiceRs.getDouble("kopl19");
                            municservDAO.rate = accLiceRs.getDouble("tusl9");
                            municservDAO.recalc = accLiceRs.getDouble("k9") + accLiceRs.getDouble("k19");
                            recalcreas1 ="";
                            if (accLiceRs.getString("pr9") != null){
                                recalcreas1 = accLiceRs.getString("pr9");
                            }
                            recalcreas2 ="";
                            if (accLiceRs.getString("pr19") != null){
                                recalcreas2 = accLiceRs.getString("pr19");
                            }
                            municservDAO.recalcsum = Math.abs(accLiceRs.getDouble("k9") + accLiceRs.getDouble("k19"));
                            municservDAO.cnsindmetd = accLiceRs.getString("mt9");
                            municservDAO.cnsobhmetd = accLiceRs.getString("mt19");
                            municservDAO.cnsindval = accLiceRs.getDouble("o9");
                            municservDAO.cnsobshval = accLiceRs.getDouble("o19");
                            //Текущие показания приборов учёта коммунальных услуг - индивидульное потребление
                            municservDAO.sinfindval = accLiceRs.getDouble("ener");
                            //Текущие показания приборов учёта коммунальных услуг - общедомовые нужды
                            municservDAO.sinfhonval = accLiceRs.getDouble("en");
                            //Суммарный объём коммунальных услуг в доме - индивидульное потребление
                            municservDAO.sinfhotin = accLiceRs.getDouble("sd9");
                            //Суммарный объём коммунальных услуг в доме - общедомовые нужды
                            municservDAO.sinfhotnee = accLiceRs.getDouble("odn9");
                            //Норматив потребления коммунальных услуг - индивидульное потребление
                            municservDAO.sinfincnor = accLiceRs.getDouble("norm9");
                            //Норматив потребления коммунальных услуг - общедомовые нужды
                            municservDAO.sinfhonnr = accLiceRs.getDouble("norm19");
                            municservDAO.insertServ();
                        } else {
                            throw new MyOwnException("electric day service in nsi51 not found");
                        }
                    }
                    //электроэнергия ночь
                    if (accLiceRs.getDouble("energ_n") != 0 || accLiceRs.getDouble("d_energn") != 0|| accLiceRs.getDouble("enn") != 0) {
                        String src = "Электроснабжение";//по этой строке будет поиск в справочнике 51
                        src = new String(src.getBytes("Cp1251"), "UTF-8");
                        ResultSet nsi51ItemRS = getNsi51Item1(accountRS.getString("orgppaguid"), src);
                        //Есть ли запись в индивидуальном справочнике 51
                        if (nsi51ItemRS.next()) {
                            municservDAO.initialize();
                            municservDAO.accoguid = accountRS.getString("accguid");
                            municservDAO.nsicode = nsi51ItemRS.getString("code");
                            municservDAO.nsiguid = nsi51ItemRS.getString("guid");
                            municservDAO.acctotal = accLiceRs.getDouble("energ_n") + accLiceRs.getDouble("d_energn");
                            municservDAO.totalpay = accLiceRs.getDouble("kopl9n") + accLiceRs.getDouble("kopl19n");
                            municservDAO.rate = accLiceRs.getDouble("tusl9n");
                            municservDAO.recalc = accLiceRs.getDouble("k9n") + accLiceRs.getDouble("k19n");
                            recalcreas1 ="";
                            if (accLiceRs.getString("pr9") != null){
                                recalcreas1 = accLiceRs.getString("pr9");
                            }
                            recalcreas2 ="";
                            if (accLiceRs.getString("pr19") != null){
                                recalcreas2 = accLiceRs.getString("pr19");
                            }
                            municservDAO.recalcsum = Math.abs(accLiceRs.getDouble("k9n") + accLiceRs.getDouble("k19n"));
                            municservDAO.cnsindmetd = accLiceRs.getString("mt9");
                            municservDAO.cnsobhmetd = accLiceRs.getString("mt19");
                            municservDAO.cnsindval = accLiceRs.getDouble("o9n");
                            municservDAO.cnsobshval = accLiceRs.getDouble("o19n");
                            //Текущие показания приборов учёта коммунальных услуг - индивидульное потребление
                            municservDAO.sinfindval = accLiceRs.getDouble("enern");
                            //Текущие показания приборов учёта коммунальных услуг - общедомовые нужды
                            municservDAO.sinfhonval = accLiceRs.getDouble("enn");
                            //Суммарный объём коммунальных услуг в доме - индивидульное потребление
                           // municservDAO.sinfhotin = accLiceRs.getDouble("sd9n");
                            //Суммарный объём коммунальных услуг в доме - общедомовые нужды
                            //municservDAO.sinfhotnee = accLiceRs.getDouble("odn19n");
                            //Норматив потребления коммунальных услуг - индивидульное потребление
                            municservDAO.sinfincnor = accLiceRs.getDouble("norm9");
                            //Норматив потребления коммунальных услуг - общедомовые нужды
                            municservDAO.sinfhonnr = accLiceRs.getDouble("norm19");
                            municservDAO.insertServ();
                        } else {
                            throw new MyOwnException("electric night service in nsi51 not found");
                        }
                    }

                } catch (MyOwnException e) {
                    //если ошибка в процессе - удаляем все из bill
                    deleteBill(accountRS.getString("accguid"));
                    deleteMunicServ(accountRS.getString("accguid"));
                    deleteJilServ(accountRS.getString("accguid"));
                    String src = e.getMessage();
                    insertAccGilcomErr(accountRS.getString("accountnum"), src);
                    database.dbfJDBCConnection();//восстанавливаем соединение с базой
                    e.printStackTrace();
                }

            }
            progress.set(progress.get() + progressDelta);
        }
        database.conn.close();
    }

    static class MyOwnException extends Exception {
        public MyOwnException(String msg) {
            super(msg);
        }
    }
}
