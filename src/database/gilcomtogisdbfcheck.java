package database;

import dataobject.liceDAO;
import javafx.beans.property.SimpleDoubleProperty;

import java.sql.Connection;
import java.sql.ResultSet;

import static dataobject.accgilcomerrDAO.deleteAccGilcomErr;
import static dataobject.accgilcomerrDAO.insertAccGilcomErr;
import static dataobject.billdocDAO.getBillByAccounNum;
import static dataobject.tsgDAO.clearTsgByGm;

/**
 * Created by dionis on 07.12.2016.
 */
public class gilcomtogisdbfcheck {
    private static double billProgressDelta;
    //положение прогрессбара
    public static volatile SimpleDoubleProperty progress;
    //ищем лицевые в gilcom\Lice которых нет в bill\billdoc
    public static void findtBadAccount() {

        try {
            deleteAccGilcomErr();
            progress.set(0.05);
            clearTsgByGm();
            progress.set(progress.get() + 0.05);
            ResultSet liceAccountRS = liceDAO.getLice();
            billProgressDelta = (0.9/ dataobject.liceDAO.getLiceCount());
            Connection conn = database.dbfJDBCConnection();
            while (liceAccountRS.next()) {
                ResultSet billdocAccount = getBillByAccounNum(liceAccountRS.getString("ln").trim());
                //в billdoc нет такого
                if (!billdocAccount.next()) {
                    String src = "Начисления нет в billdoc. Не будет отправлен в ГиС";
                    src = new String(src.getBytes("Cp1251"), "UTF-8");
                    insertAccGilcomErr(liceAccountRS.getString("ln"), src);
                }
                progress.set(progress.get() + billProgressDelta);
            }
            conn.close();

        } catch (Exception e) {
            System.err.println("Error occurred in insertBadAccount");
            e.printStackTrace();
        }
    }
}
